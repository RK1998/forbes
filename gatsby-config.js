const languages = require('./src/data/languages')
require('dotenv').config()

module.exports = {
    siteMetadata: {
        title: process.env.SITE_TITLE,
        description: process.env.SITE_DESCRIPTION,
        siteUrl: process.env.SITE_URL,
        image: process.env.SITE_IMAGE,
        organization: {
            name: process.env.SITE_TITLE,
            url: process.env.SITE_URL,
            logo: process.env.SITE_LOGO,
        },
        social: {
            twitter: process.env.SITE_TWITTER,
        },
        languages,
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        'gatsby-plugin-sass',
        `gatsby-transformer-json`,
        `gatsby-plugin-postcss`,
        `gatsby-plugin-dark-mode`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/src/data/articles`,
            },
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                path: `${__dirname}/static/img`,
                name: 'uploads',
            },
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                path: `${__dirname}/src/pages`,
                name: 'pages',
            },
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                path: `${__dirname}/src/img`,
                name: 'images',
            },
        },
        'gatsby-transformer-javascript-frontmatter',
        'gatsby-plugin-sharp',
        'gatsby-transformer-sharp',
        `gatsby-plugin-typescript`,
        // {
        //     resolve: `gatsby-plugin-nprogress`,
        //     options: {
        //         // Setting a color is optional.
        //         color: `#000000`,
        //         // Disable the loading spinner.
        //         showSpinner: false,
        //     },
        // },
        {
            resolve: 'gatsby-plugin-react-svg',
            options: {
                rule: {
                    include: /\.inline\.svg$/,
                },
            },
        },
        {
            resolve: 'gatsby-plugin-firebase',
            options: {
                credentials: {
                    apiKey: process.env.FIREBASE_API_KEY,
                    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
                    databaseURL: process.env.FIREBASE_DATABASE_URL,
                    projectId: process.env.FIREBASE_PROJECT_ID,
                    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
                    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
                    appId: process.env.FIREBASE_APP_ID,
                },
            },
        },
        // {
        //     resolve: 'gatsby-plugin-preconnect',
        //     options: {
        //         domains: [
        //             {
        //                 domain: 'https://c.disquscdn.com/',
        //                 crossOrigin: true,
        //             },
        //             {
        //                 domain: 'https://www.google-analytics.com',
        //                 crossOrigin: true,
        //             },
        //             {
        //                 domain: 'https://disqus.com',
        //                 crossOrigin: true,
        //             },
        //         ],
        //     },
        // },
        // {
        //     resolve: `gatsby-plugin-disqus`,
        //     options: {
        //         shortname: process.env.DISQUS_SHORTNAME,
        //     },
        // },
        'gatsby-plugin-preload-link-crossorigin',
        {
            resolve: `gatsby-plugin-google-adsense`,
            options: {
                publisherId: `ca-pub-1562072359497089`,
            },
        },
        {
            resolve: 'gatsby-transformer-remark',
            options: {
                plugins: [
                    {
                        resolve: 'gatsby-remark-relative-images',
                        options: {
                            name: 'uploads',
                        },
                    },
                    {
                        resolve: 'gatsby-remark-images',
                        options: {
                            maxWidth: 2048,
                        },
                    },
                    {
                        resolve: 'gatsby-remark-copy-linked-files',
                        options: {
                            destinationDir: 'static',
                        },
                    },
                ],
            },
        },
        `gatsby-plugin-netlify-cms`,
        `gatsby-plugin-netlify`,
        {
            resolve: 'gatsby-plugin-netlify-cache',
            options: {
                cachePublic: true,
            },
        },
        {
            resolve: 'gatsby-plugin-sentry',
            options: {
                dsn: process.env.SENTRY_DSN,
            },
        },
        {
            resolve: 'gatsby-plugin-purgecss', // purges all unused/unreferenced css rules
            options: {
                develop: false, // Activates purging in npm run develop
                purgeOnly: ['/all.sass'],
            },
        },
        // {
        //     resolve: `gatsby-plugin-minify-classnames`,
        //     options: {
        //         dictionary:
        //             'bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ0123456789',
        //         enable: false,
        //     },
        // },
        {
            resolve: 'gatsby-plugin-webpack-bundle-analyser-v2',
            options: {
                devMode: true,
            },
        },
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Forbes Benelux`,
                short_name: `Forbes Benelux`,
                start_url: `/`,
                background_color: `#000000`,
                theme_color: `#FFFFFF`,
                display: `standalone`,
                icon: `src/img/icon.png`,
            },
        },
        {
            resolve: 'gatsby-plugin-i18n',
            options: {
                langKeyForNull: 'any',
                langKeyDefault: 'en',
                prefixDefault: false,
                useLangKeyLayout: false,
            },
        },
        {
            resolve: 'gatsby-plugin-i18n-tags',
            options: {
                tagPage: 'src/templates/category-tags.tsx',
                tagsUrl: '/category/',
                langKeyForNull: 'any',
                langKeyDefault: 'en',
                prefixDefault: false,
            },
        },
        // {
        //     resolve: 'gatsby-plugin-html-minifier',
        //     options: {
        //         caseSensitive: false,
        //         collapseBooleanAttributes: false,
        //         useShortDoctype: false,
        //     },
        // },
        // {
        //     resolve: `gatsby-plugin-canonical-urls`,
        //     options: {
        //         siteUrl: `https://optimal.digital`,
        //     },
        // },
        'gatsby-plugin-loadable-components-ssr',
        `gatsby-plugin-offline`,
        `gatsby-plugin-scroll-reveal`,
        {
            resolve: `gatsby-plugin-sitemap`,
            options: {
                output: `/sitemap.xml`,
            },
        },
        {
            resolve: 'gatsby-plugin-robots-txt',
            options: {
                host: process.env.SITE_URL,
                sitemap: `${process.env.SITE_URL}/sitemap.xml`,
                policy: [
                    {
                        userAgent: '*',
                        allow: '/',
                    },
                ],
            },
        },
    ],
}
