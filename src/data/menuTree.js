module.exports = {
    billionaires: ['Billionaires', 'Billionaires'],
    services: ['services', 'services'],
    work: ['work', 'work'],
    hire: ['hire us', 'hire us'],
    blog: ['blog', 'blog'],
    contact: ['contact-us', 'contact-us'],
    privacy: ['privacy-policy', 'privacy-policy'],
    cookie: ['cookie-policy', 'cookie-policy'],
    jobs: ['jobs', 'jobs'],
    technologies: ['services/technologies', 'services/technologies'],
    digitalMarketing: [
        'services/digital-marketing',
        'services/cifruli-marketingi',
    ],
    webDevelopment: ['services/web-development', 'services/web-development'],
    appDevelopment: ['services/app-development', 'services/app-development'],
    blockchainDevelopment: [
        'services/blockchain-development',
        'services/blockchain-development',
    ],
    IoT: ['services/internet-of-things', 'services/nivtebis-interneti'],
}
