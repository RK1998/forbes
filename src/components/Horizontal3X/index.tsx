import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql, StaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import Time from '../Time'
import { FormattedMessage } from 'react-intl'

const switchData = (data, langKey) => {
    // eslint-disable-next-line no-unused-vars
    var posts
    switch (langKey) {
        case '':
            return (posts = data.en)
        case 'ka':
            return (posts = data.ka)
        default:
            return ' '
    }
}

class Horizontal3X extends React.Component {
    constructor(props) {
        super(props)
        this.state = { url: '' }
    }

    getUrl() {
        const urlChecker =
            window.location.pathname == '/blog/' || '/blog'
                ? ''
                : window.location.pathname
        this.setState({ url: urlChecker })
    }

    componentDidMount() {
        this.getUrl()
    }

    toObject() {
        const dateForm = this.posts.map(
            ({ node: post }) => post.frontmatter.date
        )
        var rv = {}
        for (var i = 0; i < dateForm.length; ++i) rv[i] = dateForm[i]
        return rv
    }

    render() {
        const { data } = this.props
        const langKey = this.state.url.slice(1, 3)
        const { edges: posts } = switchData(data, langKey)
        return (
            <>
                <div className="flex w-full p-5 lg:p-0 justify-center flex-col lg:flex-row ">
                    <div className="w-full p-2">
                        <div id="" className="my-2 mx-1 flex flex-wrap">
                            {posts &&
                                posts.map(({ node: post }) => (
                                    <div
                                        className="w-full lg:w-1/3 px-3 my-4"
                                        key={post.id}
                                    >
                                        <div className="" id="">
                                            <article className="">
                                                <div id="">
                                                    <Link to={post.fields.slug}>
                                                        <figure className="">
                                                            <Img
                                                                alt={
                                                                    post
                                                                        .frontmatter
                                                                        .imageAlt
                                                                }
                                                                fluid={
                                                                    post
                                                                        .frontmatter
                                                                        .image
                                                                        .childImageSharp
                                                                        .fluid
                                                                }
                                                            />
                                                        </figure>
                                                    </Link>
                                                </div>

                                                <div className="" id="">
                                                    <div className="">
                                                        <h2 className="text-2xl mt-2">
                                                            <Link
                                                                className="font-bold text-black hover:text-turmeric-600"
                                                                to={
                                                                    post.fields
                                                                        .slug
                                                                }
                                                            >
                                                                {
                                                                    post
                                                                        .frontmatter
                                                                        .title
                                                                }
                                                            </Link>
                                                        </h2>
                                                        <div className="flex items-center my-2">
                                                            <span className="mr-0">
                                                                <Time
                                                                    pubdate
                                                                    langKey={
                                                                        post
                                                                            .fields
                                                                            .langKey
                                                                    }
                                                                    date={
                                                                        post
                                                                            .frontmatter
                                                                            .date
                                                                    }
                                                                />
                                                            </span>
                                                            <span className="">
                                                                <div className="">
                                                                    <p className="border-l-2 pl-5 ml-5 text-xs text-gray-500">
                                                                        by{' '}
                                                                        {
                                                                            post
                                                                                .frontmatter
                                                                                .author
                                                                        }
                                                                    </p>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <p className="">
                                                        <Link
                                                            className="text-gray-500 hover:text-gray-500 text-sm"
                                                            to={
                                                                post.fields.slug
                                                            }
                                                        >
                                                            {post.excerpt}
                                                        </Link>
                                                    </p>

                                                    <div className="my-2">
                                                        <Link
                                                            className="text-turmeric-500 hover:text-turmeric-700 text-sm"
                                                            to={
                                                                post.fields.slug
                                                            }
                                                        >
                                                            <FormattedMessage id="blog.keep.reading" />
                                                        </Link>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                ))}
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

Horizontal3X.propTypes = {
    data: PropTypes.shape({
        allMarkdownRemark: PropTypes.shape({
            edges: PropTypes.array,
        }),
    }),
}

// eslint-disable-next-line react/display-name
export default () => (
    <StaticQuery
        query={graphql`
            query Horizontal3XQuery {
                site {
                    siteMetadata {
                        title
                        languages {
                            langs
                            defaultLangKey
                        }
                    }
                }
                en: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "blog-post" }
                            lang: { regex: "/(en|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorImage {
                                    childImageSharp {
                                        fluid(maxWidth: 32, quality: 100) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                            timeToRead
                        }
                    }
                }
                ka: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "blog-post" }
                            lang: { regex: "/(ka|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorImage {
                                    childImageSharp {
                                        fluid(maxWidth: 32, quality: 100) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `}
        render={(data) => <Horizontal3X data={data} />}
    />
)
