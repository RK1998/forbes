import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql, StaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import Time from '../Time'
import { FormattedMessage } from 'react-intl'

const switchData = (data, langKey) => {
    // eslint-disable-next-line no-unused-vars
    var posts
    switch (langKey) {
        case '':
            return (posts = data.en)
        case 'ka':
            return (posts = data.ka)
        default:
            return ' '
    }
}

class CategoryRoll1X2 extends React.Component {
    constructor(props) {
        super(props)
        this.state = { url: '' }
    }

    getUrl() {
        const urlChecker =
            window.location.pathname !== '/category/'
                ? window.location.pathname
                : ''
        this.setState({ url: urlChecker })
    }

    componentDidMount() {
        this.getUrl()
    }

    toObject() {
        const dateForm = this.posts.map(
            ({ node: post }) => post.frontmatter.date
        )
        var rv = {}
        for (var i = 0; i < dateForm.length; ++i) rv[i] = dateForm[i]
        return rv
    }

    render() {
        const { data } = this.props
        const langKey = this.state.url.slice(1, 3)
        const { edges: posts } = switchData(data, langKey)
        return (
            <>
                <section id="category" className="is-max-desktop mt-10">
                    <Link to={'#'}>
                        <h1 className="flex py-4 font-sans tracking-wide font-semibold text-lime-700 text-2xl text-left border-t-4 border-b-2 border-lime-700 mt-4 mb-2 mx-4 lg:m-2">
                            Money
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                                className="w-5 mx-2"
                            >
                                <path
                                    transform="rotate(-180 8.964 11)"
                                    fill="currentColor"
                                    d="M1 10h16v2H1z"
                                />
                                <path
                                    transform="rotate(134.999 14.965 13.124)"
                                    fill="currentColor"
                                    d="M11 12.1h8v2h-8z"
                                />
                                <path
                                    transform="rotate(-134.999 14.965 8.877)"
                                    fill="currentColor"
                                    d="M11 7.9h8v2h-8z"
                                />
                            </svg>
                        </h1>
                    </Link>
                    <div
                        className="Blogroll1X2example bg-forbes-site-white"
                        data-sal="fade-in"
                        data-sal-delay="300"
                        data-sal-easing="ease-out"
                    >
                        {posts &&
                            posts.map(({ node: post }) => (
                                <div className="m-3" key={post.id}>
                                    <div className="">
                                        <Link to={post.fields.slug}>
                                            <figure className="mb-4">
                                                <Img
                                                    alt={
                                                        post.frontmatter
                                                            .imageAlt
                                                    }
                                                    fluid={
                                                        post.frontmatter.image
                                                            .childImageSharp
                                                            .fluid
                                                    }
                                                />
                                            </figure>
                                        </Link>
                                        <h3 className="hover:underline tracking-wide">
                                            <Link
                                                className="BlogRollTitle"
                                                to={post.fields.slug}
                                            >
                                                {post.frontmatter.title}
                                            </Link>
                                        </h3>
                                        <p className="text-forbes-type-black text-xs mt-4 font-sans font-normal">
                                            By{' '}
                                            <span className="font-sans text-forbes-type-black font-medium">
                                                {post.frontmatter.author} {''}
                                            </span>
                                            <span className="text-forbes-type-black text-xs mt-4 font-sans font-normal">
                                                {
                                                    post.frontmatter
                                                        .authorPosition
                                                }
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            ))}
                    </div>
                </section>
            </>
        )
    }
}

CategoryRoll1X2.propTypes = {
    data: PropTypes.shape({
        allMarkdownRemark: PropTypes.shape({
            edges: PropTypes.array,
        }),
    }),
}

// eslint-disable-next-line react/display-name
export default () => (
    <StaticQuery
        query={graphql`
            query CategoryRoll1X2Query {
                site {
                    siteMetadata {
                        title
                        languages {
                            langs
                            defaultLangKey
                        }
                    }
                }
                en: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(en|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorPosition
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                            timeToRead
                        }
                    }
                }
                ka: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(ka|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorPosition
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `}
        render={(data) => <CategoryRoll1X2 data={data} />}
    />
)
