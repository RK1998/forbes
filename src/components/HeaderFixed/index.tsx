import React from 'react'
// import Logo from '../../img/logo.svg'
// import ModeToggler from '../ModeToggler'
// import SelectLanguage from '../SelectLanguage'
// import select from '../../components/utils'
// import menuTree from '../../data/menuTree'
// import { FormattedMessage } from 'react-intl'
import Link from 'gatsby-link'
// import './header.css'

import { useState } from 'react'

const HeaderFixed = () => {
    const [active, setActive] = useState(false)
    const [searchActive, setSearchActive] = useState(false)

    const handleClick = () => {
        setActive(!active)
    }

    const handleSearchClick = () => {
        setSearchActive(!searchActive)
        setActive(false)
    }

    return (
        <>
            <nav
                className="
                            flex 
                            items-center 
                            bg-forbes-site-black
                            py-5 
                            px-1 
                            h-16
                            xl:h-16 
                            justify-between 
                            z-50 
                            w-full"
            >
                <button
                    className={`focus:outline-none pl-3`}
                    onClick={handleClick}
                >
                    {active ? (
                        <svg
                            className="w-8 h-8 text-forbes-site-white fill-current cursor-pointer outline-none"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                        >
                            <path
                                transform="rotate(45.001 10 10)"
                                d="M2 9h16v2H2z"
                            />
                            <path
                                transform="rotate(134.999 10 10)"
                                d="M2 9h16v2H2z"
                            />
                        </svg>
                    ) : (
                        <svg
                            className="w-8 h-8 cursor-pointer outline-none text-forbes-site-white fill-current"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                        >
                            <path d="M2 5.5h16v2H2zM2 9.5h16v2H2zM2 13.5h16v2H2z" />
                        </svg>
                    )}
                </button>

                <div
                    className={`${
                        active
                            ? 'absolute z-20 top-16 w-screen -left-1 bg-transparent flex justify-start'
                            : 'hidden'
                    }`}
                >
                    <div className="flex flex-col bg-forbes-medium-black w-screen md:w-1/3 h-screen px-1">
                        <div className="dropdown">
                            <button className="text-white hover:text-turmeric-500 font-semibold py-3 px-12 rounded inline-flex items-start">
                                <span className="mr-1">Category</span>
                            </button>
                            <ul className="dropdown-menu hidden pt-0 w-full pl-0">
                                <li className="">
                                    <a
                                        className="text-gray-300 hover:text-turmeric-600 bg-forbes-medium-black py-2 pl-16 block "
                                        href="#"
                                    >
                                        Dropdown Category
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <button className="text-white hover:text-turmeric-500 font-semibold py-3 px-12 rounded inline-flex items-start">
                            <Link to="/contact-us">Contact Us</Link>
                        </button>
                        <button className="text-white hover:text-turmeric-500 font-semibold py-3 px-12 rounded inline-flex items-start">
                            <Link to="/subscribe">Subscribe</Link>
                        </button>
                        <button className="text-white hover:text-turmeric-500 font-semibold py-3 px-12 rounded inline-flex items-start">
                            <Link to="/advertising">Advertising</Link>
                        </button>
                        <button className="text-white hover:text-turmeric-500 font-semibold py-3 px-12 rounded inline-flex items-start">
                            <Link to="/forbes-press-room">
                                Forbes Press Room
                            </Link>
                        </button>
                        <button className="text-white hover:text-turmeric-500 font-semibold py-3 px-12 rounded inline-flex items-start">
                            <Link to="/privacy-policy">Privacy Policy</Link>
                        </button>
                    </div>
                </div>

                <Link
                    to="/"
                    className="m-auto pr-10 lg:pr-0 lg:absolute lg:left-16 "
                >
                    <svg
                        className="w-24 bg-forbes-site-black"
                        fill="#fff"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 200 54"
                    >
                        <path d="M113.3 18.2c0-5.8.1-11.2.4-16.2L98.4 4.9v1.4l1.5.2c1.1.1 1.8.5 2.2 1.1.4.7.7 1.7.9 3.2.2 2.9.4 9.5.3 19.9 0 10.3-.1 16.8-.3 19.3 5.5 1.2 9.8 1.7 13 1.7 6 0 10.7-1.7 14.1-5.2 3.4-3.4 5.2-8.2 5.2-14.1 0-4.7-1.3-8.6-3.9-11.7-2.6-3.1-5.9-4.6-9.8-4.6-2.6 0-5.3.7-8.3 2.1zm.3 30.8c-.2-3.2-.4-12.8-.4-28.5.9-.3 2.1-.5 3.6-.5 2.4 0 4.3 1.2 5.7 3.7 1.4 2.5 2.1 5.5 2.1 9.3 0 4.7-.8 8.5-2.4 11.7-1.6 3.1-3.6 4.7-6.1 4.7-.8-.2-1.6-.3-2.5-.4zM41 3H1v2l2.1.2c1.6.3 2.7.9 3.4 1.8.7 1 1.1 2.6 1.2 4.8.8 10.8.8 20.9 0 30.2-.2 2.2-.6 3.8-1.2 4.8-.7 1-1.8 1.6-3.4 1.8l-2.1.3v2h25.8v-2l-2.7-.2c-1.6-.2-2.7-.9-3.4-1.8-.7-1-1.1-2.6-1.2-4.8-.3-4-.5-8.6-.5-13.7l5.4.1c2.9.1 4.9 2.3 5.9 6.7h2V18.9h-2c-1 4.3-2.9 6.5-5.9 6.6l-5.4.1c0-9 .2-15.4.5-19.3h7.9c5.6 0 9.4 3.6 11.6 10.8l2.4-.7L41 3zm-4.7 30.8c0 5.2 1.5 9.5 4.4 12.9 2.9 3.4 7.2 5 12.6 5s9.8-1.7 13-5.2c3.2-3.4 4.7-7.7 4.7-12.9s-1.5-9.5-4.4-12.9c-2.9-3.4-7.2-5-12.6-5s-9.8 1.7-13 5.2c-3.2 3.4-4.7 7.7-4.7 12.9zm22.3-11.4c1.2 2.9 1.7 6.7 1.7 11.3 0 10.6-2.2 15.8-6.5 15.8-2.2 0-3.9-1.5-5.1-4.5-1.2-3-1.7-6.8-1.7-11.3C47 23.2 49.2 18 53.5 18c2.2-.1 3.9 1.4 5.1 4.4zm84.5 24.3c3.3 3.3 7.5 5 12.5 5 3.1 0 5.8-.6 8.2-1.9 2.4-1.2 4.3-2.7 5.6-4.5l-1-1.2c-2.2 1.7-4.7 2.5-7.6 2.5-4 0-7.1-1.3-9.2-4-2.2-2.7-3.2-6.1-3-10.5H170c0-4.8-1.2-8.7-3.7-11.8-2.5-3-6-4.5-10.5-4.5-5.6 0-9.9 1.8-13 5.3-3.1 3.5-4.6 7.8-4.6 12.9 0 5.2 1.6 9.4 4.9 12.7zm7.4-25.1c1.1-2.4 2.5-3.6 4.4-3.6 3 0 4.5 3.8 4.5 11.5l-10.6.2c.1-3 .6-5.7 1.7-8.1zm46.4-4c-2.7-1.2-6.1-1.9-10.2-1.9-4.2 0-7.5 1.1-10 3.2s-3.8 4.7-3.8 7.8c0 2.7.8 4.8 2.3 6.3 1.5 1.5 3.9 2.8 7 3.9 2.8 1 4.8 2 5.8 2.9 1 1 1.6 2.1 1.6 3.6 0 1.4-.5 2.7-1.6 3.7-1 1.1-2.4 1.6-4.2 1.6-4.4 0-7.7-3.2-10-9.6l-1.7.5.4 10c3.6 1.4 7.6 2.1 12 2.1 4.6 0 8.1-1 10.7-3.1 2.6-2 3.9-4.9 3.9-8.5 0-2.4-.6-4.4-1.9-5.9-1.3-1.5-3.4-2.8-6.4-4-3.3-1.2-5.6-2.3-6.8-3.3-1.2-1-1.8-2.2-1.8-3.7s.4-2.7 1.3-3.7 2-1.4 3.4-1.4c4 0 6.9 2.9 8.7 8.6l1.7-.5-.4-8.6zm-96.2-.9c-1.4-.7-2.9-1-4.6-1-1.7 0-3.4.7-5.3 2.1-1.9 1.4-3.3 3.3-4.4 5.9l.1-8-15.2 3v1.4l1.5.1c1.9.2 3 1.7 3.2 4.4.6 6.2.6 12.8 0 19.8-.2 2.7-1.3 4.1-3.2 4.4l-1.5.2v1.9h21.2V49l-2.7-.2c-1.9-.2-3-1.7-3.2-4.4-.6-5.8-.7-12-.2-18.4.6-1 1.9-1.6 3.9-1.8 2-.2 4.3.4 6.7 1.8l3.7-9.3z"></path>
                    </svg>
                </Link>
                <div
                    className="order-3 pr-3 absolute right-1 z-50"
                    onClick={handleSearchClick}
                >
                    {searchActive ? (
                        <svg
                            className="w-8 h-8 text-forbes-site-white fill-current cursor-pointer outline-none"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                        >
                            <path
                                transform="rotate(45.001 10 10)"
                                d="M2 9h16v2H2z"
                            />
                            <path
                                transform="rotate(134.999 10 10)"
                                d="M2 9h16v2H2z"
                            />
                        </svg>
                    ) : (
                        <svg
                            className="w-5 text-forbes-site-white fill-current cursor-pointer"
                            aria-hidden="true"
                            focusable="false"
                            data-prefix="fas"
                            data-icon="search"
                            role="img"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512"
                        >
                            <path
                                fill="currentColor"
                                d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"
                            ></path>
                        </svg>
                    )}
                </div>
                <div
                    className={`${
                        searchActive ? '' : 'hidden'
                    }   absolute w-full left-0 top-0 z-20`}
                >
                    <input
                        type="text"
                        name="search-input"
                        id="search-input inpu1"
                        placeholder="Tap enter to search"
                        className="w-full bg-forbes-site-black text-forbes-site-white h-16
                        xl:h-16 placeholder-gray-400 pl-5 pr-10 text-2xl font-sans"
                    />
                </div>
            </nav>
        </>
    )
}

export default HeaderFixed
