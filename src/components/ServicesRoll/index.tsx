import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql, StaticQuery } from 'gatsby'
import { FormattedMessage } from 'react-intl'

const switchData = (data, langKey) => {
    // eslint-disable-next-line no-unused-vars
    var posts
    switch (langKey) {
        case '':
            return (posts = data.en)
        case 'ka':
            return (posts = data.ka)
        default:
            return ' '
    }
}

class ServicesRoll extends React.Component {
    constructor(props) {
        super(props)
        this.state = { url: '' }
    }

    getUrl() {
        const urlChecker =
            window.location.pathname !== '/services/'
                ? window.location.pathname
                : ''
        this.setState({ url: urlChecker })
    }

    componentDidMount() {
        this.getUrl()
    }

    toObject() {
        const dateForm = this.posts.map(
            ({ node: post }) => post.frontmatter.date
        )
        var rv = {}
        for (var i = 0; i < dateForm.length; ++i) rv[i] = dateForm[i]
        return rv
    }

    render() {
        const { data } = this.props
        const langKey = this.state.url.slice(1, 3)
        const { edges: posts } = switchData(data, langKey)
        return (
            <div className="">
                {posts &&
                    posts.map(({ node: post }) => (
                        <div className="">
                            <div className="column is-6 is-size-3 is-title-color is-services-margin has-text-weight-medium">
                                {post.frontmatter.number}
                                <h2 className="is-size-5-rem has-text-weight-normal is-services-margin is-size-2-mobile">
                                    {post.frontmatter.title}
                                </h2>
                            </div>
                            <div className="column is-6">
                                <p className="has-text-weight-medium is-subtitle-color">
                                    {post.frontmatter.description}
                                </p>
                                <Link
                                    className="has-text-primary is-size-4 mt-4"
                                    to={post.fields.slug}
                                >
                                    <FormattedMessage id="services.roll.cta" />
                                </Link>
                            </div>
                        </div>
                    ))}
            </div>
        )
    }
}

ServicesRoll.propTypes = {
    data: PropTypes.shape({
        allMarkdownRemark: PropTypes.shape({
            edges: PropTypes.array,
        }),
    }),
}

// eslint-disable-next-line react/display-name
export default () => (
    <StaticQuery
        query={graphql`
            query ServicesRollQuery {
                site {
                    siteMetadata {
                        title
                        languages {
                            langs
                            defaultLangKey
                        }
                    }
                }
                en: allMarkdownRemark(
                    sort: { order: ASC, fields: [frontmatter___number] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "services-post" }
                            lang: { regex: "/(en|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                title
                                description
                                templateKey
                                lang
                                number
                            }
                        }
                    }
                }
                ka: allMarkdownRemark(
                    sort: { order: ASC, fields: [frontmatter___number] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "services-post" }
                            lang: { regex: "/(ka|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                title
                                description
                                templateKey
                                lang
                                number
                            }
                        }
                    }
                }
            }
        `}
        render={(data) => <ServicesRoll data={data} />}
    />
)
