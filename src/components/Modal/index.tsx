import React from 'react'
import { PropTypes } from 'prop-types'

const Modal = ({ children, closeModal, modalState }) => {
    if (!modalState) {
        return null
    }

    return (
        <div className="modal opacity-50 z-50 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
            {children}
        </div>
    )
}

Modal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    modalState: PropTypes.bool.isRequired,
}

export default Modal
