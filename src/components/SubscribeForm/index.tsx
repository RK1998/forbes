import React from 'react'
import { injectIntl, FormattedMessage } from 'react-intl'

class SubscribeForm extends React.Component {
    render() {
        const { intl } = this.props

        return (
            <div className="has-text-centered">
                <div className="column is-8 is-offset-2">
                    <h1 className="is-size-1 is-title-color">
                        <FormattedMessage id="blog.roll.title" />
                    </h1>
                    <h2 className="is-size-5 mt-3 is-subtitle-color">
                        <FormattedMessage id="blog.roll.subtitle" />
                    </h2>
                    <div className="column is-8 is-offset-2">
                        <div className="field">
                            <div className="control is-expanded">
                                <input
                                    className="input has-input-style"
                                    type="text"
                                    placeholder={intl.formatMessage({
                                        id: 'blog.roll.subscribe.placeholder',
                                    })}
                                />
                            </div>

                            <div className="control mt-3">
                                <a className="button is-primary has-text-weight-medium is-outlined">
                                    <FormattedMessage id="blog.submit" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    langKey() {
        throw new Error('Method not implemented.')
    }
}

export default injectIntl(SubscribeForm)
