import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import kebabCase from 'lodash/kebabCase'

const Tags = ({ tags, langKey }) => {
    return (
        <div>
            {tags && tags.length ? (
                <div>
                    {tags.map((tag) => (
                        <span key={tag + `tag`}>
                            <Link
                                className="text-green-600 text-xs hover:text-green-600 font-sans font-normal"
                                to={`/${
                                    langKey === 'en' ? '' : langKey
                                }/category/${kebabCase(tag)}/`}
                            >
                                {tag}
                            </Link>
                        </span>
                    ))}
                </div>
            ) : null}
        </div>
    )
}

Tags.propTypes = {
    tags: PropTypes.array,
    langKey: PropTypes.string,
}

export default Tags
