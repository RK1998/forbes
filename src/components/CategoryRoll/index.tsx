import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql, StaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import Time from '../Time'
import { FormattedMessage } from 'react-intl'
import AdPremium from '../AdPremium/index'
import AdNonPremium from '../AdNonPremium/index'
import Horizontal3X from '../Horizontal3X'

const switchData = (data, langKey) => {
    // eslint-disable-next-line no-unused-vars
    var posts
    switch (langKey) {
        case '':
            return (posts = data.en)
        case 'ka':
            return (posts = data.ka)
        default:
            return ' '
    }
}

class CategoryRoll extends React.Component {
    constructor(props) {
        super(props)
        this.state = { url: '' }
    }

    getUrl() {
        const urlChecker =
            window.location.pathname == '/category/' || '/category'
                ? ''
                : window.location.pathname
        this.setState({ url: urlChecker })
    }

    componentDidMount() {
        this.getUrl()
    }

    toObject() {
        const dateForm = this.posts.map(
            ({ node: post }) => post.frontmatter.date
        )
        var rv = {}
        for (var i = 0; i < dateForm.length; ++i) rv[i] = dateForm[i]
        return rv
    }

    render() {
        const { data } = this.props
        const langKey = this.state.url.slice(1, 3)
        const { edges: posts } = switchData(data, langKey)
        return (
            <>
                <AdPremium></AdPremium>
                <div className="container mx-auto" id="main-containers">
                    <div className="flex w-full p-5 lg:p-0 justify-center flex-col lg:flex-row ">
                        <div className="w-full p-2">
                            {/* <div className="w-full lg:w-9/12 p-2"> */}
                            <Link to={'#'}>
                                <h1 className="flex py-4 font-sans tracking-wide font-semibold text-lime-700 text-2xl text-left border-b-2 border-turmeric-400 mt-4 mb-2 mx-4 lg:m-2">
                                    Category Title
                                    <svg
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="fal"
                                        data-icon="long-arrow-right"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                        data-fa-i2svg=""
                                        className="w-5 mx-2"
                                    >
                                        <path
                                            fill="currentColor"
                                            d="M311.03 131.515l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L387.887 239H12c-6.627 0-12 5.373-12 12v10c0 6.627 5.373 12 12 12h375.887l-83.928 83.444c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l116.485-116c4.686-4.686 4.686-12.284 0-16.971L328 131.515c-4.686-4.687-12.284-4.687-16.97 0z"
                                        />
                                    </svg>
                                </h1>
                            </Link>
                            <div
                                id="mainTwoBogroll1"
                                className="my-2 mx-1 flex flex-wrap "
                            >
                                {posts &&
                                    posts.map(({ node: post }) => (
                                        <div
                                            className="w-full lg:w-1/3 px-3 my-4"
                                            key={post.id}
                                        >
                                            <div className="" id="ListParent">
                                                <article className="">
                                                    <div id="row-flexed">
                                                        <Link
                                                            className="font-serif font-bold tracking-wide "
                                                            to={
                                                                post.fields.slug
                                                            }
                                                        >
                                                            <figure className="">
                                                                <Img
                                                                    alt={
                                                                        post
                                                                            .frontmatter
                                                                            .imageAlt
                                                                    }
                                                                    fluid={
                                                                        post
                                                                            .frontmatter
                                                                            .image
                                                                            .childImageSharp
                                                                            .fluid
                                                                    }
                                                                />
                                                            </figure>
                                                        </Link>
                                                    </div>

                                                    <div
                                                        className=""
                                                        id="row-reversed"
                                                    >
                                                        <div className="">
                                                            <h2 className="text-2xl lg:text-3xl mt-2 tracking-wide	 font-serif font-bold">
                                                                <Link
                                                                    className="tracking-wide	 font-serif font-bold text-black hover:text-turmeric-600"
                                                                    to={
                                                                        post
                                                                            .fields
                                                                            .slug
                                                                    }
                                                                >
                                                                    {
                                                                        post
                                                                            .frontmatter
                                                                            .title
                                                                    }
                                                                </Link>
                                                            </h2>
                                                            <div className="flex items-center my-2">
                                                                <span className="mr-0 font-sans font-normal">
                                                                    <Time
                                                                        pubdate
                                                                        langKey={
                                                                            post
                                                                                .fields
                                                                                .langKey
                                                                        }
                                                                        date={
                                                                            post
                                                                                .frontmatter
                                                                                .date
                                                                        }
                                                                    />
                                                                </span>
                                                                <span className="">
                                                                    <div className="">
                                                                        <p className="font-sans font-normal border-l-2 pl-5 ml-5 text-xs text-gray-500">
                                                                            by{' '}
                                                                            {
                                                                                post
                                                                                    .frontmatter
                                                                                    .author
                                                                            }
                                                                        </p>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <p className="">
                                                            <Link
                                                                className="font-geo font-normal text-gray-500 hover:text-gray-500 text-sm"
                                                                to={
                                                                    post.fields
                                                                        .slug
                                                                }
                                                            >
                                                                {post.excerpt}
                                                            </Link>
                                                        </p>

                                                        <div className="my-2">
                                                            <Link
                                                                className="font-sans font-normal text-turmeric-500 hover:text-turmeric-700 text-sm"
                                                                to={
                                                                    post.fields
                                                                        .slug
                                                                }
                                                            >
                                                                <FormattedMessage id="blog.keep.reading" />
                                                            </Link>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        </div>
                                    ))}
                            </div>
                            <hr className="my-5 bg-gray-200" />
                            <Horizontal3X />
                            <Horizontal3X />
                            <div className="flex justify-center">
                                <AdNonPremium></AdNonPremium>
                            </div>
                            <Horizontal3X />
                            <Horizontal3X />
                        </div>
                    </div>
                </div>
                <AdPremium></AdPremium>
            </>
        )
    }
}

CategoryRoll.propTypes = {
    data: PropTypes.shape({
        allMarkdownRemark: PropTypes.shape({
            edges: PropTypes.array,
        }),
    }),
}

// eslint-disable-next-line react/display-name
export default () => (
    <StaticQuery
        query={graphql`
            query CategoryRollQuery {
                site {
                    siteMetadata {
                        title
                        languages {
                            langs
                            defaultLangKey
                        }
                    }
                }
                en: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(en|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorImage {
                                    childImageSharp {
                                        fluid(maxWidth: 32, quality: 100) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                            timeToRead
                        }
                    }
                }
                ka: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(ka|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorImage {
                                    childImageSharp {
                                        fluid(maxWidth: 32, quality: 100) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `}
        render={(data) => <CategoryRoll data={data} />}
    />
)
