import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql, StaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import Time from '../Time'
import { FormattedMessage } from 'react-intl'

const switchData = (data, langKey) => {
    // eslint-disable-next-line no-unused-vars
    var posts
    switch (langKey) {
        case '':
            return (posts = data.en)
        case 'ka':
            return (posts = data.ka)
        default:
            return ' '
    }
}

class CategoryRollVertical1X2 extends React.Component {
    constructor(props) {
        super(props)
        this.state = { url: '' }
    }

    getUrl() {
        const urlChecker =
            window.location.pathname !== '/category/'
                ? window.location.pathname
                : ''
        this.setState({ url: urlChecker })
    }

    componentDidMount() {
        this.getUrl()
    }

    toObject() {
        const dateForm = this.posts.map(
            ({ node: post }) => post.frontmatter.date
        )
        var rv = {}
        for (var i = 0; i < dateForm.length; ++i) rv[i] = dateForm[i]
        return rv
    }

    render() {
        const { data } = this.props
        const langKey = this.state.url.slice(1, 3)
        const { edges: posts } = switchData(data, langKey)
        return (
            <div className="BlogRollVertical1X2example bg-forbes-site-white  lg:grid ">
                {posts &&
                    posts.map(({ node: post }) => (
                        <div className="m-4" key={post.id}>
                            <div className="">
                                <article className="">
                                    <Link to={post.fields.slug}>
                                        <figure className="">
                                            <Img
                                                alt={post.frontmatter.imageAlt}
                                                fluid={
                                                    post.frontmatter.image
                                                        .childImageSharp.fluid
                                                }
                                            />
                                        </figure>
                                    </Link>
                                    <div className="">
                                        <div className=""></div>
                                        <h2 className="mt-2 tracking-wide	 font-serif font-bold">
                                            <Link
                                                className="text-2xl lg:text-3xl text-black font-serif font-bold"
                                                to={post.fields.slug}
                                            >
                                                {post.frontmatter.title}
                                            </Link>
                                        </h2>
                                        <p className="text-gray-500 text-md my-2 font-geo font-normal">
                                            <Link
                                                className="text-gray-500 font-geo font-normal"
                                                to={post.fields.slug}
                                            >
                                                {post.excerpt}
                                            </Link>
                                        </p>
                                        <div className="">
                                            <div className="">
                                                <div className="mt-1 flex items-center justify-between flex-nowrap">
                                                    <div className="">
                                                        <Time
                                                            pubdate
                                                            langKey={
                                                                post.fields
                                                                    .langKey
                                                            }
                                                            date={
                                                                post.frontmatter
                                                                    .date
                                                            }
                                                        />
                                                    </div>
                                                    <p className="text-gray-500">
                                                        author:{' '}
                                                        {
                                                            post.frontmatter
                                                                .author
                                                        }
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mb-5">
                                            <Link
                                                className="text-turmeric-500 hover:text-turmeric-600"
                                                to={post.fields.slug}
                                            >
                                                <FormattedMessage id="blog.keep.reading" />
                                            </Link>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    ))}
            </div>
        )
    }
}

CategoryRollVertical1X2.propTypes = {
    data: PropTypes.shape({
        allMarkdownRemark: PropTypes.shape({
            edges: PropTypes.array,
        }),
    }),
}

// eslint-disable-next-line react/display-name
export default () => (
    <StaticQuery
        query={graphql`
            query CategoryRollVertical1X2Query {
                site {
                    siteMetadata {
                        title
                        languages {
                            langs
                            defaultLangKey
                        }
                    }
                }
                en: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(en|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorImage {
                                    childImageSharp {
                                        fluid(maxWidth: 32, quality: 100) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                            timeToRead
                        }
                    }
                }
                ka: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(ka|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorImage {
                                    childImageSharp {
                                        fluid(maxWidth: 32, quality: 100) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `}
        render={(data) => <CategoryRollVertical1X2 data={data} />}
    />
)
