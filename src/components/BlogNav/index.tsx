import React from 'react'
import PropTypes from 'prop-types'
import { graphql, StaticQuery } from 'gatsby'

const switchData = (data: { en: any; ka: any }, langKey: any) => {
    // eslint-disable-next-line no-unused-vars
    var posts: any
    switch (langKey) {
        case 'en':
            return (posts = data.en)
        case 'ka':
            return (posts = data.ka)
        default:
            return ' '
    }
}

class CategoryNav extends React.Component {
    posts: any
    static propTypes: {
        data: PropTypes.Requireable<
            PropTypes.InferProps<{
                allMarkdownRemark: PropTypes.Requireable<
                    PropTypes.InferProps<{
                        edges: PropTypes.Requireable<any[]>
                    }>
                >
            }>
        >
    }
    constructor(props: Readonly<{}>) {
        super(props)
        this.state = { url: '' }
    }

    getUrl() {
        const urlChecker = window.location.pathname.includes('/ka/category/')
            ? window.location.pathname
            : '/en/category/'
        this.setState({ url: urlChecker })
    }

    componentDidMount() {
        this.getUrl()
    }

    toObject() {
        const dateForm = this.posts.map(
            ({ node: post }) => post.frontmatter.date
        )
        var rv = {}
        for (var i = 0; i < dateForm.length; ++i) rv[i] = dateForm[i]
        return rv
    }

    render() {
        var data: any
        if (this.props.data !== null) {
            data = this.props.data
        }
        const langKey = this.state.url.slice(1, 3)
        const langFixer = langKey === 'en' ? '' : 'ka'
        const posts = switchData(data, langKey)
        const post = posts.group
        return (
            <div className="is-max-desktop mt-10">
                {post &&
                    post.map((tag: { fieldValue: {} }) => (
                        <div className=" items-center py-4 border-t-4 border-b-2 border-lime-700 mt-4 mb-2 mx-4 lg:m-2">
                            <h1 className="flex font-sans tracking-wide font-semibold text-lime-700 text-2xl text-left">
                                {tag.fieldValue}

                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20"
                                    className="w-5 mx-2"
                                >
                                    <path
                                        transform="rotate(-180 8.964 11)"
                                        fill="currentColor"
                                        d="M1 10h16v2H1z"
                                    />
                                    <path
                                        transform="rotate(134.999 14.965 13.124)"
                                        fill="currentColor"
                                        d="M11 12.1h8v2h-8z"
                                    />
                                    <path
                                        transform="rotate(-134.999 14.965 8.877)"
                                        fill="currentColor"
                                        d="M11 7.9h8v2h-8z"
                                    />
                                </svg>
                            </h1>
                        </div>
                    ))}
            </div>
        )
    }
}

CategoryNav.propTypes = {
    data: PropTypes.shape({
        allMarkdownRemark: PropTypes.shape({
            edges: PropTypes.array,
        }),
    }),
}

// eslint-disable-next-line react/display-name
export default () => (
    <StaticQuery
        query={graphql`
            query CategoryNavQuery {
                site {
                    siteMetadata {
                        title
                        languages {
                            langs
                            defaultLangKey
                        }
                    }
                }
                en: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(en|any)/" }
                        }
                    }
                ) {
                    group(field: frontmatter___tags) {
                        fieldValue
                    }
                }
                ka: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(ka|any)/" }
                        }
                    }
                ) {
                    group(field: frontmatter___tags) {
                        fieldValue
                    }
                }
            }
        `}
        render={(data) => <CategoryNav key={data.id} data={data} />}
    />
)
