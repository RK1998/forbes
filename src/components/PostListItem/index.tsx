import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Img from 'gatsby-image'

const PostListItem = ({ post }) => {
    return (
        <div id="category" className="is-max-tablet mt-10">
            <div
                className="Blogroll1X2example bg-forbes-site-white"
                data-sal="fade-in"
                data-sal-delay="300"
            >
                <div className="m-3" key={post.id}>
                    <div className="">
                        <Link to={post.fields.slug}>
                            <figure className="mb-4">
                                <Img
                                    alt={post.frontmatter.imageAlt}
                                    fluid={
                                        post.frontmatter.image.childImageSharp
                                            .fluid
                                    }
                                />
                            </figure>
                        </Link>
                        <h3 className="hover:underline tracking-wide">
                            <Link
                                className="BlogRollTitle"
                                to={post.fields.slug}
                            >
                                {post.frontmatter.title}
                            </Link>
                        </h3>
                        <p>{post.frontmatter.excerpt}</p>
                        <p className="text-forbes-type-black text-xs mt-4 font-sans font-normal">
                            By{' '}
                            <span className="font-sans text-forbes-type-black font-medium">
                                {post.frontmatter.author} {''}
                            </span>
                            <span className="text-forbes-type-black text-xs mt-4 font-sans font-normal">
                                {post.frontmatter.authorPosition}
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

PostListItem.propTypes = {
    post: PropTypes.shape({
        id: PropTypes.string,
        fields: PropTypes.shape({
            slug: PropTypes.string.isRequired,
            langKey: PropTypes.string.isRequired,
        }),
        frontmatter: PropTypes.shape({
            title: PropTypes.string.isRequired,
            author: PropTypes.string.isRequired,
            imageAlt: PropTypes.string.isRequired,
            authorImage: PropTypes.object.isRequired,
            image: PropTypes.object.isRequired,
            tag: PropTypes.array,
            tags: PropTypes.array,
            date: PropTypes.string.isRequired,
        }),
        excerpt: PropTypes.string.isRequired,
        timeToRead: PropTypes.number,
    }),
}

export default PostListItem
