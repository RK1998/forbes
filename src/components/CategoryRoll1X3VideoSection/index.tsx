import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql, StaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import { FormattedMessage } from 'react-intl'
import ReactPlayer from 'react-player'
import Modal from '../Modal'

const switchData = (data, langKey) => {
    // eslint-disable-next-line no-unused-vars
    var posts
    switch (langKey) {
        case '':
            return (posts = data.en)
        case 'ka':
            return (posts = data.ka)
        default:
            return ' '
    }
}

class CategoryRoll1X3VideoSection extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            url: '',
            modalState: false,
            playingVD: false,
        }
        this.toggleModal = this.toggleModal.bind(this)
    }

    toggleModal() {
        this.setState((prev) => {
            const newState = !prev.modalState
            return { modalState: newState }
        })
        this.setState({ playingVD: true })
    }

    getUrl() {
        const urlChecker =
            window.location.pathname !== '/category/'
                ? window.location.pathname
                : ''
        this.setState({ url: urlChecker })
    }

    componentDidMount() {
        this.getUrl()
    }

    toObject() {
        const dateForm = this.posts.map(
            ({ node: post }) => post.frontmatter.date
        )
        var rv = {}
        for (var i = 0; i < dateForm.length; ++i) rv[i] = dateForm[i]
        return rv
    }

    render() {
        const { data } = this.props
        const langKey = this.state.url.slice(1, 3)
        const { edges: posts } = switchData(data, langKey)
        const { playingVD } = this.state

        return (
            <>
                <section id="category" className="is-max-desktop mt-10">
                    <Link to={'#'}>
                        <h1 className="flex py-4 font-sans tracking-wide font-semibold text-lime-700 text-2xl text-left border-t-4 border-b-2 border-lime-700 mt-4 mb-2 mx-4 lg:m-2">
                            Forbes TV
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                                className="w-5 mx-2"
                            >
                                <path
                                    transform="rotate(-180 8.964 11)"
                                    fill="currentColor"
                                    d="M1 10h16v2H1z"
                                />
                                <path
                                    transform="rotate(134.999 14.965 13.124)"
                                    fill="currentColor"
                                    d="M11 12.1h8v2h-8z"
                                />
                                <path
                                    transform="rotate(-134.999 14.965 8.877)"
                                    fill="currentColor"
                                    d="M11 7.9h8v2h-8z"
                                />
                            </svg>
                        </h1>
                    </Link>
                    <div className="grid grid-cols-2 gap-4 bg-forbes-site-white">
                        {posts &&
                            posts.map(({ node: post }) => (
                                <div className="m-3" key={post.id}>
                                    <div className="">
                                        <figure
                                            className="mb-4"
                                            onClick={this.toggleModal}
                                        >
                                            <Img
                                                alt={post.frontmatter.imageAlt}
                                                fluid={
                                                    post.frontmatter.image
                                                        .childImageSharp.fluid
                                                }
                                            />
                                        </figure>
                                        <h3
                                            className="hover:underline tracking-wide BlogRollTitle"
                                            onClick={this.toggleModal}
                                        >
                                            {post.frontmatter.title}
                                        </h3>
                                        <Modal
                                            closeModal={this.toggleModal}
                                            modalState={this.state.modalState}
                                        >
                                            <ReactPlayer
                                                playing={playingVD}
                                                url="https://www.youtube.com/watch?v=YLKNhybUztU"
                                            />
                                        </Modal>
                                    </div>
                                </div>
                            ))}
                    </div>
                </section>
            </>
            // <section id="forbesTV" className="mt-10">
            //     <Link to={'#'}>
            //         <h1 className="flex py-4 font-sans tracking-wide font-semibold text-lime-700 text-2xl text-left border-t-4 border-b-2 border-lime-700 mt-4 mb-2 mx-4 lg:m-2">
            //             Forbes TV
            //             <svg
            //                 aria-hidden="true"
            //                 focusable="false"
            //                 data-prefix="fal"
            //                 data-icon="long-arrow-right"
            //                 role="img"
            //                 xmlns="http://www.w3.org/2000/svg"
            //                 viewBox="0 0 448 512"
            //                 data-fa-i2svg=""
            //                 className="w-5 mx-2"
            //             >
            //                 <path
            //                     fill="currentColor"
            //                     d="M311.03 131.515l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L387.887 239H12c-6.627 0-12 5.373-12 12v10c0 6.627 5.373 12 12 12h375.887l-83.928 83.444c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l116.485-116c4.686-4.686 4.686-12.284 0-16.971L328 131.515c-4.686-4.687-12.284-4.687-16.97 0z"
            //                 />
            //             </svg>
            //         </h1>
            //     </Link>
            //     <div
            //         className="BlogRoll1X3VideoSection bg-forbes-site-white  lg:grid m-4"
            //         style={{ background: `#e2e2e2` }}
            //     >
            //         {posts &&
            //             posts.map(({ node: post }) => (
            //                 <div className="m-4 parentArticle" key={post.id}>
            //                     <div className="flex ">
            //                         <article className="w-full flex items-start">

            //                             <div className="w-1/2 order-1">
            //                                 <div className=""></div>
            //                                 <h2 className="mt-2 tracking-wide Blogroll1X2example">
            //                                     <Link
            //                                         className="BlogRollTitle"
            //                                         to={post.fields.slug}
            //                                     >
            //                                         {post.frontmatter.title}
            //                                     </Link>
            //                                 </h2>
            //                                 <ReactPlayer
            //         className='react-player'
            //         url="https://www.youtube.com/watch?v=YLKNhybUztU"
            //       />
            //                                 {/* <div className="">
            //                                 <div className="">
            //                                     <div className="mt-1 flex items-center justify-between flex-nowrap">
            //                                         <div className="">
            //                                             <Time
            //                                                 pubdate
            //                                                 langKey={
            //                                                     post.fields
            //                                                         .langKey
            //                                                 }
            //                                                 date={
            //                                                     post.frontmatter
            //                                                         .date
            //                                                 }
            //                                             />
            //                                         </div>
            //                                         <p className="text-gray-500">
            //                                             author:{' '}
            //                                             {
            //                                                 post.frontmatter
            //                                                     .author
            //                                             }
            //                                         </p>
            //                                     </div>
            //                                 </div>
            //                             </div> */}
            //                                 {/* <div className="mb-5">
            //                                 <Link
            //                                     className="text-turmeric-500 hover:text-turmeric-600"
            //                                     to={post.fields.slug}
            //                                 >
            //                                     <FormattedMessage id="blog.keep.reading" />
            //                                 </Link>
            //                             </div> */}
            //                             </div>
            //                         </article>
            //                     </div>
            //                 </div>
            //             ))}
            //     </div>
            // </section>
        )
    }
}

CategoryRoll1X3VideoSection.propTypes = {
    data: PropTypes.shape({
        allMarkdownRemark: PropTypes.shape({
            edges: PropTypes.array,
        }),
    }),
}

// eslint-disable-next-line react/display-name
export default () => (
    <StaticQuery
        query={graphql`
            query CategoryRoll1X3VideoSectioQuery {
                site {
                    siteMetadata {
                        title
                        languages {
                            langs
                            defaultLangKey
                        }
                    }
                }
                en: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(en|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorImage {
                                    childImageSharp {
                                        fluid(maxWidth: 32, quality: 100) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                            timeToRead
                        }
                    }
                }
                ka: allMarkdownRemark(
                    sort: { order: DESC, fields: [frontmatter___date] }
                    filter: {
                        frontmatter: {
                            templateKey: { eq: "category-post" }
                            lang: { regex: "/(ka|any)/" }
                        }
                    }
                ) {
                    edges {
                        node {
                            excerpt(pruneLength: 150)
                            id
                            fields {
                                slug
                            }
                            frontmatter {
                                author
                                authorImage {
                                    childImageSharp {
                                        fluid(maxWidth: 32, quality: 100) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                                title
                                templateKey
                                date
                                lang
                                tags
                                imageAlt
                                image {
                                    childImageSharp {
                                        fluid(maxWidth: 500) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `}
        render={(data) => <CategoryRoll1X3VideoSection data={data} />}
    />
)
