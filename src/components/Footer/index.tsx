import React from 'react'
import { Link } from 'gatsby'
import { injectIntl } from 'react-intl'
// import menuTree from '../../data/menuTree'
import select from '../../components/utils'
import FaLinkedin from '@meronex/icons/fa/FaLinkedin'
import FaFacebookSquare from '@meronex/icons/fa/FaFacebookSquare'
import FaInstagramSquare from '@meronex/icons/fa/FaInstagramSquare'
import FaYoutubeSquare from '@meronex/icons/fa/FaYoutubeSquare'
import Release1 from '../../../static/img/releases/1.jpg'
import Release2 from '../../../static/img/releases/2.jpg'
import Release3 from '../../../static/img/releases/3.jpg'
import Release4 from '../../../static/img/releases/4.jpg'
import Release5 from '../../../static/img/releases/5.jpg'
import Release6 from '../../../static/img/releases/6.jpg'
class Footer extends React.Component {
    render() {
        const props = this.props
        const sel = select(props.langKey)
        const lang = `${props.langKey !== 'en' ? props.langKey : ''}`
        return (
            <footer className="bg-forbes-medium-black w-full flex-col font-serif font-semibold">
                <div className="flex justify-start sm:justify-evenly py-12 flex-wrap lg:flex-nowrap">
                    <div className="w-6/12 lg:w-3/12 mx-5">
                        <div className="flex justify-start sm:justify-center mb-10">
                            <Link to="/">
                                <div className="footer-logo">
                                    <svg
                                        className="w-28 bg-forbes-medium-black"
                                        fill="#fff"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 200 54"
                                    >
                                        <path d="M113.3 18.2c0-5.8.1-11.2.4-16.2L98.4 4.9v1.4l1.5.2c1.1.1 1.8.5 2.2 1.1.4.7.7 1.7.9 3.2.2 2.9.4 9.5.3 19.9 0 10.3-.1 16.8-.3 19.3 5.5 1.2 9.8 1.7 13 1.7 6 0 10.7-1.7 14.1-5.2 3.4-3.4 5.2-8.2 5.2-14.1 0-4.7-1.3-8.6-3.9-11.7-2.6-3.1-5.9-4.6-9.8-4.6-2.6 0-5.3.7-8.3 2.1zm.3 30.8c-.2-3.2-.4-12.8-.4-28.5.9-.3 2.1-.5 3.6-.5 2.4 0 4.3 1.2 5.7 3.7 1.4 2.5 2.1 5.5 2.1 9.3 0 4.7-.8 8.5-2.4 11.7-1.6 3.1-3.6 4.7-6.1 4.7-.8-.2-1.6-.3-2.5-.4zM41 3H1v2l2.1.2c1.6.3 2.7.9 3.4 1.8.7 1 1.1 2.6 1.2 4.8.8 10.8.8 20.9 0 30.2-.2 2.2-.6 3.8-1.2 4.8-.7 1-1.8 1.6-3.4 1.8l-2.1.3v2h25.8v-2l-2.7-.2c-1.6-.2-2.7-.9-3.4-1.8-.7-1-1.1-2.6-1.2-4.8-.3-4-.5-8.6-.5-13.7l5.4.1c2.9.1 4.9 2.3 5.9 6.7h2V18.9h-2c-1 4.3-2.9 6.5-5.9 6.6l-5.4.1c0-9 .2-15.4.5-19.3h7.9c5.6 0 9.4 3.6 11.6 10.8l2.4-.7L41 3zm-4.7 30.8c0 5.2 1.5 9.5 4.4 12.9 2.9 3.4 7.2 5 12.6 5s9.8-1.7 13-5.2c3.2-3.4 4.7-7.7 4.7-12.9s-1.5-9.5-4.4-12.9c-2.9-3.4-7.2-5-12.6-5s-9.8 1.7-13 5.2c-3.2 3.4-4.7 7.7-4.7 12.9zm22.3-11.4c1.2 2.9 1.7 6.7 1.7 11.3 0 10.6-2.2 15.8-6.5 15.8-2.2 0-3.9-1.5-5.1-4.5-1.2-3-1.7-6.8-1.7-11.3C47 23.2 49.2 18 53.5 18c2.2-.1 3.9 1.4 5.1 4.4zm84.5 24.3c3.3 3.3 7.5 5 12.5 5 3.1 0 5.8-.6 8.2-1.9 2.4-1.2 4.3-2.7 5.6-4.5l-1-1.2c-2.2 1.7-4.7 2.5-7.6 2.5-4 0-7.1-1.3-9.2-4-2.2-2.7-3.2-6.1-3-10.5H170c0-4.8-1.2-8.7-3.7-11.8-2.5-3-6-4.5-10.5-4.5-5.6 0-9.9 1.8-13 5.3-3.1 3.5-4.6 7.8-4.6 12.9 0 5.2 1.6 9.4 4.9 12.7zm7.4-25.1c1.1-2.4 2.5-3.6 4.4-3.6 3 0 4.5 3.8 4.5 11.5l-10.6.2c.1-3 .6-5.7 1.7-8.1zm46.4-4c-2.7-1.2-6.1-1.9-10.2-1.9-4.2 0-7.5 1.1-10 3.2s-3.8 4.7-3.8 7.8c0 2.7.8 4.8 2.3 6.3 1.5 1.5 3.9 2.8 7 3.9 2.8 1 4.8 2 5.8 2.9 1 1 1.6 2.1 1.6 3.6 0 1.4-.5 2.7-1.6 3.7-1 1.1-2.4 1.6-4.2 1.6-4.4 0-7.7-3.2-10-9.6l-1.7.5.4 10c3.6 1.4 7.6 2.1 12 2.1 4.6 0 8.1-1 10.7-3.1 2.6-2 3.9-4.9 3.9-8.5 0-2.4-.6-4.4-1.9-5.9-1.3-1.5-3.4-2.8-6.4-4-3.3-1.2-5.6-2.3-6.8-3.3-1.2-1-1.8-2.2-1.8-3.7s.4-2.7 1.3-3.7 2-1.4 3.4-1.4c4 0 6.9 2.9 8.7 8.6l1.7-.5-.4-8.6zm-96.2-.9c-1.4-.7-2.9-1-4.6-1-1.7 0-3.4.7-5.3 2.1-1.9 1.4-3.3 3.3-4.4 5.9l.1-8-15.2 3v1.4l1.5.1c1.9.2 3 1.7 3.2 4.4.6 6.2.6 12.8 0 19.8-.2 2.7-1.3 4.1-3.2 4.4l-1.5.2v1.9h21.2V49l-2.7-.2c-1.9-.2-3-1.7-3.2-4.4-.6-5.8-.7-12-.2-18.4.6-1 1.9-1.6 3.9-1.8 2-.2 4.3.4 6.7 1.8l3.7-9.3z"></path>
                                    </svg>
                                </div>
                            </Link>
                        </div>
                    </div>

                    <div className="w-12/12 sm:w-6/12 lg:w-3/12 border-gray-400 border-t-2 mx-5 mb-10">
                        <div
                            id="social-and-address"
                            className="pt-5 flex flex-col justify-start"
                        >
                            <div
                                id="social-icons"
                                className="border-gray-400 border-b-2 pb-5"
                            >
                                <div className="flex">
                                    <a title="facebook" href="">
                                        <FaFacebookSquare
                                            className="mx-2 text-white"
                                            size="2.5rem"
                                        />
                                    </a>
                                    <a title="twitter" href="">
                                        <FaInstagramSquare
                                            className="mx-2 text-white"
                                            size="2.5rem"
                                        />
                                    </a>
                                    <a title="linkedin" href="">
                                        <FaLinkedin
                                            className="mx-2 text-white"
                                            size="2.5rem"
                                        />
                                    </a>
                                    <a title="linkedin" href="">
                                        <FaYoutubeSquare
                                            className="mx-2 text-white"
                                            size="2.5rem"
                                        />
                                    </a>
                                </div>
                            </div>
                            <div id="find-us" className="pt-4 text-gray-300">
                                <p>lorem</p>
                                <p>lorem ipsum</p>
                                <p>lorem ipsum lorem</p>
                                <p>lorem ipsum lorem ipsum</p>
                                <br />
                                <p>lorem ipsum lorem ipsum</p>
                            </div>
                        </div>
                    </div>

                    <div className="w-12/12 lg:w-6/12 border-gray-400 border-t-2 mx-5">
                        <div
                            id="releases"
                            className="flex flex-wrap justify-evenly pt-5"
                        >
                            <div id="image-container-release">
                                <img
                                    className="w-20 sm:w-36 mx-6 mb-4"
                                    src={Release1}
                                    alt="release"
                                />
                            </div>
                            <div id="image-container-release">
                                <img
                                    className="w-20 sm:w-36 mx-6 mb-4"
                                    src={Release2}
                                    alt="release"
                                />
                            </div>
                            <div id="image-container-release">
                                <img
                                    className="w-20 sm:w-36 mx-6 mb-4"
                                    src={Release3}
                                    alt="release"
                                />
                            </div>
                            <div id="image-container-release">
                                <img
                                    className="w-20 sm:w-36 mx-6 mb-4"
                                    src={Release4}
                                    alt="release"
                                />
                            </div>
                            <div id="image-container-release">
                                <img
                                    className="w-20 sm:w-36 mx-6 mb-4"
                                    src={Release5}
                                    alt="release"
                                />
                            </div>
                            <div id="image-container-release">
                                <img
                                    className="w-20 sm:w-36 mx-6 mb-4"
                                    src={Release6}
                                    alt="release"
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <div
                    id="conditions"
                    className="w-full p-5 flex justify-center lg:justify-end"
                >
                    <div id="" className="w-12/12 lg:w-9/12">
                        <ul className="flex justify-start flex-wrap border-gray-400 border-t-2 border-b-2 py-4 mb-5">
                            <li className="mx-2 text-sm py-1">
                                <Link
                                    className="text-gray-400 hover:text-gray-300"
                                    to="/privacy-policy"
                                >
                                    Privacy Policy
                                </Link>
                            </li>

                            <li className="mx-2 text-sm py-1">
                                <Link
                                    className="text-gray-400 hover:text-gray-300"
                                    to="/contact-us"
                                >
                                    Contact Us
                                </Link>
                            </li>

                            <li className="mx-2 text-sm py-1">
                                <Link
                                    className="text-gray-400 hover:text-gray-300"
                                    to="/forbes-press-room"
                                >
                                    Forbes Press Room
                                </Link>
                            </li>
                            <li className="mx-2 text-sm py-1">
                                <Link
                                    className="text-gray-400 hover:text-gray-300"
                                    to="/subscribe"
                                >
                                    Subscribe
                                </Link>
                            </li>
                            <li className="mx-2 text-sm py-1">
                                <Link
                                    className="text-gray-400 hover:text-gray-300"
                                    to="/advertising"
                                >
                                    Advertising
                                </Link>
                            </li>
                        </ul>

                        <div className="flex flex-col text-white">
                            <span>
                                © 2021 Forbes Benelux. All Rights Reserved
                            </span>
                            <span className="my-3 text-xs">Lorem</span>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default injectIntl(Footer)
