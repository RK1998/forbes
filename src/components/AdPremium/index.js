import React from 'react'

function AdPremium() {
    return (
        <div className="w-full bg-forbes-light-gray">
            <div className="h-12" id="sticky-whitespace"></div>
            <div className="h-60 bg-forbes-light-gray"></div>
        </div>
    )
}

export default AdPremium
