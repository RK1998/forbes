/* eslint-disable react/jsx-pascal-case */
import React from 'react'
import PropTypes from 'prop-types'
import Layout from '../components/Layout'
import CategoryRoll from '../components/CategoryRoll'
import BlogNav from '../components/BlogNav'
import SEO from '../components/SEO/SEO'
import { FormattedMessage } from 'react-intl'
import { graphql } from 'gatsby'
import BlogNewsletterForm from '../components/NewsletterForm/blog-subscribe'

class CategoryIndexPage extends React.Component {
    render() {
        const data = this.props.data
        const location = this.props.location
        const jsonData = data.allArticlesJson.edges[0].node.articles
        return (
            <Layout data={data} jsonData={jsonData} location={location}>
                <SEO frontmatter={data.markdownRemark.frontmatter} />
                <CategoryRoll />
            </Layout>
        )
    }
}

CategoryIndexPage.propTypes = {
    data: PropTypes.object,
    location: PropTypes.shape({
        pathname: PropTypes.string.isRequired,
    }).isRequired,
}

export default CategoryIndexPage

export const pageQuery = graphql`
    query CategoryIndex($id: String!) {
        site {
            siteMetadata {
                title
                languages {
                    langs
                    defaultLangKey
                }
            }
        }
        allArticlesJson(filter: { title: { eq: "home" } }) {
            edges {
                node {
                    articles {
                        en
                        ka
                    }
                }
            }
        }
        markdownRemark(id: { eq: $id }) {
            id
            html
            frontmatter {
                id
                date
                title
                description
                tags
                lang
            }
        }
    }
`
