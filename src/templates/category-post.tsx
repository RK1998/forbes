/* eslint-disable react/jsx-pascal-case */
import React from 'react'
import PropTypes from 'prop-types'
import Img from 'gatsby-image'
import Tags from '../components/Tags'
import SEO from '../components/SEO/SEO'
import { graphql } from 'gatsby'
import Time from '../components/Time'
import ViewCounter from '../components/ViewCounter'
import Layout from '../components/Layout'
import Content, { HTMLContent } from '../components/Content'
import AdPremium from '../components/AdPremium'

export const CategoryPostTemplate = ({
    date,
    content,
    contentComponent,
    description,
    tags,
    title,
    langKey,
    image,
    imageAlt,
    author,
    authorQuote,
    authorLinkedIn,
    authorPosition,
    authorImage,
    slug,
    url,
    twitterHandle,
}) => {
    const PostContent = contentComponent || Content
    const socialConfig = {
        twitterHandle,
        config: {
            url: `${url}${slug}`,
            title,
            description,
        },
    }
    return (
        <section className="bg-forbes-site-white">
            <AdPremium />
            <div className="flex flex-col justify-center items-center">
                <div className="max-w-screen-md">
                    <div className="w-full mt-16">
                        <div
                            className="container is-max-desktop px-2"
                            id="article-section"
                        >
                            <div className="flex px-2 lg:px-5">
                                <div className="flex mr-2">
                                    <svg
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="far"
                                        data-icon="eye"
                                        className="w-3 text-forbes-dark-gray mr-1"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                    >
                                        <path
                                            fill="currentColor"
                                            d="M288 144a110.94 110.94 0 0 0-31.24 5 55.4 55.4 0 0 1 7.24 27 56 56 0 0 1-56 56 55.4 55.4 0 0 1-27-7.24A111.71 111.71 0 1 0 288 144zm284.52 97.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400c-98.65 0-189.09-55-237.93-144C98.91 167 189.34 112 288 112s189.09 55 237.93 144C477.1 345 386.66 400 288 400z"
                                        ></path>
                                    </svg>
                                    <ViewCounter id="CategoryPostTemplate" />
                                </div>
                                <div className="ml-2 font-sans font-normal">
                                    <Time
                                        pubdate
                                        langKey={langKey}
                                        date={date}
                                    />
                                </div>
                            </div>

                            <div className="px-2 lg:px-5">
                                <div className="mt-5 mb-4">
                                    <h1 className="text-2xl tracking-wide xl:text-4xl text-forbes-type-black font-serif font-bold tracking-wide	">
                                        {title}
                                    </h1>
                                </div>

                                <div className="my-3 flex items-center">
                                    <figure className="w-16 mr-2">
                                        <Img
                                            className="rounded-full"
                                            fluid={authorImage}
                                        />
                                    </figure>
                                    <div className="flex-col">
                                        <div className="flex flex-row items-center">
                                            <p className="text-forbes-type-black text-sm mr-2 font-sans font-bold">
                                                {author}
                                            </p>
                                            <p className="text-forbes-dark-gray text-xs font-sans font-normal">
                                                {authorPosition}
                                            </p>
                                        </div>
                                        <Tags tags={tags} langKey={langKey} />
                                        <div>
                                            <h3 className="mt-2 text-forbes-dark-gray text-xs font-serif font-normal italic">
                                                {authorQuote}
                                            </h3>
                                        </div>
                                    </div>
                                </div>

                                <div className="mt-6">
                                    <Img alt={imageAlt} fluid={image} />
                                </div>

                                <div className="mt-4 font-geo font-normal">
                                    <PostContent
                                        className="font-geo font-normal"
                                        content={content}
                                    />
                                </div>

                                <div className="my-8 items-center">
                                    <div className="flex pt-6 items-center border-forbes-dark-gray border-t-2 pb-10">
                                        <figure className="w-16 mr-2">
                                            <Img
                                                className="rounded-full"
                                                fluid={authorImage}
                                                aspectRatio={16 / 9}
                                            />
                                        </figure>
                                        <div className="flex-col">
                                            <div className="flex flex-row">
                                                <p className="text-forbes-type-black text-sm mr-2 font-sans font-bold">
                                                    {author}
                                                </p>
                                                <p className="text-forbes-dark-gray text-xs font-sans font-normal">
                                                    {authorPosition}
                                                </p>
                                            </div>
                                            <Tags
                                                tags={tags}
                                                langKey={langKey}
                                            />
                                            <div>
                                                <h3 className="mt-2 text-forbes-dark-gray text-xs font-serif font-normal italic">
                                                    {authorQuote}
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <AdPremium />
        </section>
    )
}

CategoryPostTemplate.propTypes = {
    content: PropTypes.node.isRequired,
    contentComponent: PropTypes.func,
    description: PropTypes.string,
    title: PropTypes.string,
    helmet: PropTypes.object,
    location: PropTypes.string,
    tags: PropTypes.array,
    langKey: PropTypes.string,
    image: PropTypes.object,
    imageAlt: PropTypes.string,
    author: PropTypes.string,
    authorLinkedIn: PropTypes.string,
    authorPosition: PropTypes.string,
    date: PropTypes.string,
    authorQuote: PropTypes.string,
    slug: PropTypes.string,
    authorImage: PropTypes.object,
    timeToRead: PropTypes.number,
    data: PropTypes.object,
    url: PropTypes.string,
    twitterHandle: PropTypes.string,
}

const CategoryPost = ({ data, location }) => {
    const { markdownRemark: post } = data
    const jsonData = data.allArticlesJson.edges[0].node.articles
    const langKey = post.frontmatter.lang
    const image = post.frontmatter.image.childImageSharp.fluid
    const meta = data.site.siteMetadata
    return (
        <Layout
            className="container"
            data={data}
            jsonData={jsonData}
            location={location}
        >
            <SEO frontmatter={post.frontmatter} postImage={image} isBlogPost />
            <CategoryPostTemplate
                content={post.html}
                contentComponent={HTMLContent}
                description={post.frontmatter.description}
                image={image}
                author={post.frontmatter.author}
                timeToRead={post.timeToRead}
                authorImage={post.frontmatter.authorImage.childImageSharp.fluid}
                authorQuote={post.frontmatter.authorQuote}
                authorPosition={post.frontmatter.authorPosition}
                authorLinkedIn={post.frontmatter.authorLinkedIn}
                tags={post.frontmatter.tags}
                title={post.frontmatter.title}
                date={post.frontmatter.date}
                slug={post.frontmatter.slug}
                url={meta.siteUrl}
                twitterHandle={meta.social.twitter}
                langKey={langKey}
            />
        </Layout>
    )
}

CategoryPost.propTypes = {
    author: PropTypes.string,
    imageAlt: PropTypes.string,
    data: PropTypes.shape({
        markdownRemark: PropTypes.object,
        site: PropTypes.object,
        allArticlesJson: PropTypes.object,
    }),
    location: PropTypes.shape({
        pathname: PropTypes.string.isRequired,
    }).isRequired,
}

export default CategoryPost

export const pageQuery = graphql`
    query CategoryPostByID($id: String!) {
        site {
            siteMetadata {
                social {
                    twitter
                }
                siteUrl
                title
                languages {
                    langs
                    defaultLangKey
                }
            }
        }
        allArticlesJson(filter: { title: { eq: "home" } }) {
            edges {
                node {
                    articles {
                        en
                        ka
                    }
                }
            }
        }
        markdownRemark(id: { eq: $id }) {
            id
            html
            timeToRead
            frontmatter {
                slug
                id
                title
                imageAlt
                image {
                    childImageSharp {
                        fluid(maxWidth: 1200) {
                            ...GatsbyImageSharpFluid
                        }
                    }
                }
                description
                date
                tags
                lang
                author
                authorLinkedIn
                authorPosition
                authorQuote
                authorImage {
                    childImageSharp {
                        fluid(maxWidth: 64, quality: 100) {
                            ...GatsbyImageSharpFluid
                        }
                    }
                }
            }
        }
    }
`
