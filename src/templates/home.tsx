/* eslint-disable react/jsx-pascal-case */
import React from 'react'
import * as PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Layout from '../components/Layout'
import Helmet from 'react-helmet'
import { FormattedMessage } from 'react-intl'
import select from '../components/utils'
// import menuTree from '../data/menuTree'
import Link from 'gatsby-link'

import SEO from '../components/SEO/SEO'
import { HTMLContent } from '../components/Content'
// import BlogRoll from '../components/BlogRoll'
// import PostList from '../components/PostList'
import CategoryRoll1X2 from '../components/CategoryRoll1X2'
import CategoryRoll1X3VideoSection from '../components/CategoryRoll1X3VideoSection'
import AdPremium from '../components/AdPremium'

const HomePageTemplate = ({ helmet, langFix, sel }) => {
    return (
        <div className="bg-forbes-site-white">
            {helmet || ''}
            <AdPremium />
            <CategoryRoll1X2 />
            <CategoryRoll1X3VideoSection />
            <AdPremium></AdPremium>
            <CategoryRoll1X2 />
            <CategoryRoll1X2 />
            <AdPremium />
        </div>
    )
}

HomePageTemplate.propTypes = {
    helmet: PropTypes.object,
}

class HomePage extends React.Component {
    render() {
        let data
        let dataMarkdown = []
        if (this.props.data !== null) {
            dataMarkdown = this.props.data.markdownRemark
            data = this.props.data
        }
        const jsonData = data.allArticlesJson.edges[0].node.articles
        const langKey = dataMarkdown.frontmatter.lang
        const { frontmatter } = data.markdownRemark
        const langFix = langKey === 'en' ? '' : 'ka'
        const sel = select(langKey)

        return (
            <Layout
                className=""
                data={this.props.data}
                jsonData={jsonData}
                location={this.props.location}
            >
                <SEO frontmatter={frontmatter} />
                <div>
                    <HomePageTemplate
                        helmet={
                            <Helmet
                                titleTemplate={`${dataMarkdown.frontmatter.title}`}
                            >
                                <title>{`${dataMarkdown.frontmatter.title}`}</title>
                                <meta
                                    name="description"
                                    content={`${dataMarkdown.frontmatter.description}`}
                                />
                            </Helmet>
                        }
                        imageCardSL={dataMarkdown.frontmatter.imageCardSL}
                        title={dataMarkdown.frontmatter.title}
                        main={dataMarkdown.frontmatter.main}
                        contentComponent={HTMLContent}
                        content={dataMarkdown.html}
                        langKey={langKey}
                        langFix={langFix}
                        sel={sel}
                    />
                </div>
            </Layout>
        )
    }
}

HomePage.propTypes = {
    data: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
}

export default HomePage

export const pageQuery = graphql`
    query HomePageQuery($id: String!) {
        site {
            siteMetadata {
                languages {
                    defaultLangKey
                    langs
                }
            }
        }
        allArticlesJson(filter: { title: { eq: "home" } }) {
            edges {
                node {
                    articles {
                        en
                        ka
                    }
                }
            }
        }
        markdownRemark(id: { eq: $id }) {
            html
            frontmatter {
                id
                title
                description
                lang
            }
            fields {
                slug
            }
        }
    }
`
