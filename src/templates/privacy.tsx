/* eslint-disable react/jsx-pascal-case */
import React from 'react'
import * as PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Layout from '../components/LayoutFixed'
import SEO from '../components/SEO/SEO'
import Content, { HTMLContent } from '../components/Content'

const PrivacyPageTemplate = ({ content, contentComponent }) => {
    const PageContent = contentComponent || Content
    return (
        <div className="flex flex-col justify-center bg-white items-center">
            <div className="max-w-screen-md lg:px-10 mt-10 mx-4">
                <PageContent className="content" content={content} />
            </div>
        </div>
    )
}

PrivacyPageTemplate.propTypes = {
    content: PropTypes.string,
    contentComponent: PropTypes.func,
    tags: PropTypes.array,
    langKey: PropTypes.string,
}

class PrivacyPage extends React.Component {
    render() {
        var dataMarkdown = []
        if (this.props.data !== null) {
            dataMarkdown = this.props.data.markdownRemark
        }
        const jsonData = this.props.data.allArticlesJson.edges[0].node.articles
        const { frontmatter } = dataMarkdown
        const langKey = frontmatter.lang
        const tags = frontmatter.tags
        return (
            <Layout
                data={this.props.data}
                jsonData={jsonData}
                location={this.props.location}
            >
                <SEO frontmatter={frontmatter} />
                <div>
                    <PrivacyPageTemplate
                        contentComponent={HTMLContent}
                        content={dataMarkdown.html}
                        tags={tags}
                        langKey={langKey}
                    />
                </div>
            </Layout>
        )
    }
}

PrivacyPage.propTypes = {
    data: PropTypes.object.isRequired,
    location: PropTypes.object,
}

export default PrivacyPage

export const pageQuery = graphql`
    query PrivacyPageQuery($id: String!) {
        site {
            siteMetadata {
                languages {
                    defaultLangKey
                    langs
                }
            }
        }
        allArticlesJson(filter: { title: { eq: "home" } }) {
            edges {
                node {
                    articles {
                        en
                        ka
                    }
                }
            }
        }
        markdownRemark(id: { eq: $id }) {
            html
            frontmatter {
                id
                title
                description
                tags
                lang
            }
            fields {
                slug
            }
        }
    }
`
