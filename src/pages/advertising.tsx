import React from 'react'
import Layout from '../components/LayoutFixed'
import PropTypes from 'prop-types'
import { graphql, Link } from 'gatsby'
import imageSrc from '../../static/img/woman.jpg'
import IosPaper from '@meronex/icons/ios/IosPaper'

const AdvertisingPage = ({ data, location }) => {
    const jsonData = data.allArticlesJson.edges[0].node.articles
    return (
        <Layout data={data} jsonData={jsonData} location={location}>
            <>
                <div className="container">
                    <div className="mx-5 lg:px-14">
                        <div id="advertising" className="w-full relative">
                            <div id="top-image" className="relative">
                                <img
                                    src={imageSrc}
                                    alt=""
                                    className="w-full h-auto select-none"
                                />
                                <div
                                    id="logo"
                                    className="absolute top-5 left-5"
                                >
                                    <Link
                                        to="/"
                                        className="lg:absolute lg:left-10"
                                    >
                                        <svg
                                            className="w-48"
                                            version="1.1"
                                            style={{ fill: `white` }}
                                            viewBox="0 0 240.63 33.07"
                                        >
                                            <g id="Layer_x0020_1">
                                                <metadata id="CorelCorpID_0Corel-Layer" />
                                                <path
                                                    className="fil0"
                                                    d="M63.84 16.03c-3.06,-2.06 -6.36,-1.16 -7.04,-0.04 -0.24,3.7 -0.18,9.37 0.07,12.22 0.17,1.89 0.71,2.71 2.12,2.91l1.83 0.12 0 1.28 -14.04 0 0 -1.28 0.99 -0.12c1.41,-0.2 1.95,-1.02 2.12,-2.91 0.27,-3.1 0.31,-9.55 0,-13.16 -0.17,-1.89 -0.71,-2.71 -2.12,-2.91l-0.99 -0.13 0 -0.91 10.08 -1.95 -0.11 5.27c2.36,-5.42 7.01,-6.14 9.56,-4.61l-2.47 6.22z"
                                                />
                                                <path
                                                    className="fil0"
                                                    d="M26.75 9.66l-1.58 0.44c-1.37,-4.6 -3.56,-7.2 -7.66,-7.2l-5.26 0c-0.24,2.93 -0.33,7.81 -0.31,12.9l3.54 -0.11c2.35,-0.08 3.33,-1.82 3.9,-4.41l1.32 0 0 10.77 -1.32 0c-0.57,-2.59 -1.55,-4.33 -3.9,-4.41l-3.53 -0.11c0.04,3.86 0.14,7.15 0.3,9.1 0.24,2.87 1.02,4.13 3.05,4.42l1.8 0.19 0 1.28 -17.1 0 0 -1.28 1.41 -0.19c2.03,-0.29 2.81,-1.55 3.05,-4.42 0.39,-4.72 0.46,-14.54 0,-20.02 -0.24,-2.87 -1.02,-4.13 -3.05,-4.42l-1.41 -0.19 0 -1.29 26.61 0 0.14 8.95z"
                                                />
                                                <path
                                                    className="fil0"
                                                    d="M130.56 16.21l-1.16 0.3c-1.23,-3.99 -3.11,-5.7 -5.85,-5.7 -1.83,0 -3.15,1.37 -3.15,3.37 0,1.97 1.49,3.02 5.76,4.67 3.95,1.53 5.51,3.29 5.51,6.5 0,4.77 -3.66,7.72 -9.69,7.72 -2.89,0 -6.1,-0.65 -7.99,-1.41l-0.2 -6.61 1.16 -0.31c1.66,4.63 3.81,6.31 6.66,6.31 2.42,0 3.77,-1.72 3.77,-3.56 0,-1.78 -1.01,-2.85 -4.87,-4.23 -3.59,-1.28 -6.18,-2.86 -6.18,-6.86 0,-3.9 3.45,-7.23 9.14,-7.23 2.64,0 5.15,0.46 6.83,1.23l0.26 5.81z"
                                                />
                                                <path
                                                    className="fil1"
                                                    d="M91.23 21.33c-0.01,-6.26 3.94,-12.15 11.74,-12.15 6.35,0 9.37,4.71 9.41,10.86l-14.16 0c-0.15,5.58 2.67,9.68 8.08,9.68 2.38,0 3.67,-0.59 5.1,-1.69l0.67 0.79c-1.57,2.18 -4.85,4.24 -9.2,4.24 -6.81,0 -11.63,-4.83 -11.64,-11.73zm7.05 -2.88l7.09 -0.16c0.04,-3.14 -0.46,-7.62 -3.02,-7.62 -2.57,0 -4.01,4.24 -4.07,7.78z"
                                                />
                                                <path
                                                    className="fil1"
                                                    d="M80.2 9.42c-2.09,0 -3.76,0.57 -5.5,1.42 0.02,-4.92 0.08,-9.22 0.2,-10.84l-10.12 1.95 0 0.92 0.99 0.12c1.41,0.2 1.95,1.02 2.12,2.9 0.31,3.61 0.27,22.98 0,26.09 2.72,0.61 5.67,1.08 8.62,1.08 8.03,0 12.86,-4.96 12.86,-12.81 0,-6.3 -3.92,-10.83 -9.17,-10.83zm-3.53 22.04c-0.57,0 -1.31,-0.1 -1.72,-0.17 -0.16,-2.18 -0.26,-11.14 -0.25,-18.97 0.93,-0.31 1.55,-0.39 2.36,-0.39 3.33,0 5.19,3.84 5.2,8.69 0.01,6.16 -2.29,10.84 -5.59,10.84z"
                                                />
                                                <path
                                                    className="fil1"
                                                    d="M35.26 9.17c7.56,0 11.31,5.13 11.31,11.89 0,6.6 -4.24,12 -11.8,12 -7.56,0 -11.32,-5.12 -11.32,-11.88 0,-6.6 4.24,-12.01 11.81,-12.01zm-0.4 1.46c-3.34,0 -4.28,4.52 -4.28,10.49 0,5.8 1.48,10.48 4.58,10.48 3.35,0 4.28,-4.52 4.28,-10.48 0,-5.8 -1.48,-10.49 -4.58,-10.49z"
                                                />
                                                <path
                                                    className="fil1"
                                                    d="M157.1 10.43l-1.27 0 -9.08 20.64 -9.06 -20.64 -1.25 0 0 22.08 1.19 0 0 -19.32 8.49 19.32 1.3 0 8.47 -19.32 0 19.32 1.21 0 0 -22.08zm10.05 5.27c-4.48,0 -8.17,3.89 -8.17,8.66 0,4.71 3.69,8.52 8.17,8.52 4.52,0 8.22,-3.81 8.22,-8.52 0,-4.77 -3.7,-8.66 -8.22,-8.66zm0 16.08c-3.84,0 -7,-3.35 -7,-7.42 0,-4.17 3.16,-7.54 7,-7.54 3.89,0 7.01,3.37 7.01,7.54 0,4.07 -3.12,7.42 -7.01,7.42zm23.55 -9.17c0,-4.26 -2.48,-6.91 -6.68,-6.91 -2.61,0 -4.74,1.25 -5.92,3.19l0 -2.77 -1.17 0 0 16.39 1.17 0 0 -9.9c0,-3.31 2.58,-5.76 5.89,-5.76 3.59,0 5.54,2.16 5.54,5.76l0 9.9 1.17 0 0 -9.9zm16.2 9.9c-0.27,-0.64 -0.48,-1.85 -0.48,-2.67l0 -9.27c0,-3.08 -2.36,-4.87 -6.46,-4.87 -3.7,0 -6.14,1.69 -6.87,4.62l1.21 0c0.69,-2.23 2.67,-3.47 5.66,-3.47 2.42,0 5.28,0.7 5.28,3.72 0,2.01 -2.42,2.39 -6.05,2.74 -3.5,0.32 -6.97,1.18 -6.97,4.9 0,3.15 3.09,4.58 5.95,4.58 3.25,0 5.77,-1.14 7.07,-2.99l0 0.32c0,1.01 0.16,1.78 0.45,2.39l1.21 0zm-1.66 -5.96c0,3.48 -3.53,5.06 -7.07,5.06 -1.62,0 -4.68,-0.73 -4.68,-3.43 0,-2.26 1.63,-3.38 5.83,-3.79 2.26,-0.19 4.64,-0.41 5.92,-1.59l0 3.75zm18.46 0.87l-1.25 0c-0.98,2.63 -3.3,4.29 -6.33,4.29 -3.84,0 -7,-3.31 -7,-7.35 0,-4.1 3.16,-7.45 7,-7.45 2.93,0 5.26,1.66 6.3,4.27l1.28 0c-1.11,-3.31 -3.95,-5.48 -7.58,-5.48 -4.52,0 -8.17,3.89 -8.17,8.66 0,4.71 3.65,8.52 8.17,8.52 3.63,0 6.49,-2.16 7.58,-5.46zm8.71 -11.72c-4.48,0 -8.17,3.89 -8.17,8.66 0,4.71 3.69,8.52 8.17,8.52 4.52,0 8.22,-3.81 8.22,-8.52 0,-4.77 -3.7,-8.66 -8.22,-8.66zm0 16.08c-3.84,0 -6.99,-3.35 -6.99,-7.42 0,-4.17 3.15,-7.54 6.99,-7.54 3.89,0 7.01,3.37 7.01,7.54 0,4.07 -3.12,7.42 -7.01,7.42z"
                                                />
                                            </g>
                                        </svg>
                                    </Link>
                                </div>
                                <div className="w-28 h-1 bg-white absolute bottom-36 left-1 md:left-10 lg:left-16 rounded-sm hidden md:block"></div>
                                <h1 className="absolute z-10 bottom-16 left-5 md:left-10 lg:left-16 text-white text-xl font-bold sm:text-3xl md:text-4xl lg:text-6xl">
                                    ADVERTISING
                                </h1>
                                <p className="absolute z-10 bottom-10 left-5 md:left-10 lg:left-16 text-white italic text-sm">
                                    Align with success, innovation and
                                    excellence.
                                </p>
                            </div>
                            <div className="w-full h-auto bg-black py-14">
                                <div className="w-full pl-5 py-5">
                                    <h2 className="w-full text-white md:text-4xl text-xl md:pl-5 lg:pl-10 pb-2">
                                        Forbes Magazine, Monaco Edition
                                    </h2>
                                    <div className="w-40 md:w-72 lg:w-76 bg-white h-2 rounded-sm md:ml-5 lg:ml-10"></div>
                                </div>
                                <div className="flex pl-5 pb-5 flex-wrap items-center">
                                    <div className="w-full md:w-8/12">
                                        <h3 className="text-white md:pl-5 lg:pl-10 pt-3 text-xl pr-5 md:text-3xl font-serif font-bold">
                                            The Largest & Most Influential
                                            Audience
                                        </h3>
                                        <p className="text-white text-sm italic font-serif md:pl-5 lg:pl-10">
                                            Tradition of Authoritative Business
                                            Journalism
                                        </p>
                                    </div>
                                    <div className="w-full md:w-4/12 text-white mt-10 md:mt-0">
                                        <h4>19k</h4>
                                        <span>Adult Readership</span>
                                    </div>
                                </div>
                            </div>
                            <div id="focus" className="my-10 px-10 pl-5">
                                <p className="my-3 md:pl-5 lg:pl-10 leading-8">
                                    <strong>
                                        Forbes places a focus on the individual
                                    </strong>{' '}
                                    and the ideas that have the power to change
                                    the world for the better. For over 100
                                    years, our agenda-setting cover stories have
                                    offered business transparency, inspiration
                                    and surfaced disruptive new directions. From
                                    Rockefeller to Bill Gates, the Great
                                    Depression to the Great Recession, Forbes
                                    has always provided our readers with
                                    critical insights.
                                </p>
                                <p className="my-3 md:pl-5 lg:pl-10 leading-8">
                                    Forbes Magazine offers the Monaco
                                    influential community critical business
                                    insight. Each editorial department provides
                                    readers with a fresh perspective and a
                                    provocative point of view, keeping our
                                    audience of business leaders and affluent
                                    consumers informed and engaged.
                                </p>
                            </div>
                            <div
                                id="list"
                                className="bg-black py-10 px-10 pl-5 text-white"
                            >
                                <p className="font-serif md:pl-5 lg:pl-10 my-4 text-xl">
                                    FORBES MONACO MAGAZINE{' '}
                                    <strong className="text-3xl text-white">
                                        ranks # 1
                                    </strong>
                                </p>
                                <p className="md:pl-5 lg:pl-10 mb-3">
                                    {' '}
                                    in the business competitive set, reaching
                                    the most influential and engaged audience of
                                    Monaco Residents and Visitors
                                </p>
                                <ul className="md:pl-5 lg:pl-10 list-disc ml-4">
                                    <li>Millenials</li>
                                    <li>C-Suite & Top Management</li>
                                    <li>Business Owners</li>
                                    <li>IT Decision Makers</li>
                                    <li>Business Decision Makers</li>
                                    <li>UHNWIs</li>
                                </ul>
                            </div>
                            <div
                                id="campaign"
                                className="bg-turmeric-600 flex flex-wrap py-14 px-10 pl-5"
                            >
                                <div
                                    id="planCamp"
                                    className="w-full text-center lg:w-3/12 md:pl-5 lg:pl-10"
                                >
                                    <h1 className="text-black font-bold text-4xl py-5">
                                        Plain Your Campaign
                                    </h1>
                                    <div className="w-40 h-2 bg-black rounded-sm mx-auto"></div>
                                </div>
                                <div className="w-full lg:w-9/12 flex flex-col md:flex-row justify-around py-5 md:pl-5 lg:pl-10 ">
                                    <div className="flex-col px-5 text-center md:text-left py-3">
                                        <IosPaper className="text-white mx-auto md:ml-0 text-4xl"></IosPaper>
                                        <a
                                            className="text-black text-2xl hover:text-black"
                                            href="#"
                                        >
                                            2021 Media Kit
                                        </a>
                                    </div>
                                    <div className="flex-col px-5 text-center md:text-left py-3">
                                        <IosPaper className="text-white mx-auto md:ml-0 text-4xl"></IosPaper>
                                        <a
                                            className="text-black text-2xl hover:text-black"
                                            href="#"
                                        >
                                            2021 Digital Editorial Calendar
                                        </a>
                                    </div>
                                    <div className="flex-col px-5 text-center md:text-left py-3">
                                        <IosPaper className="text-white mx-auto md:ml-0 text-4xl"></IosPaper>
                                        <a
                                            className="text-black text-2xl hover:text-black"
                                            href="#"
                                        >
                                            2021 Print Editorial Calendar
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        </Layout>
    )
}

AdvertisingPage.propTypes = {
    data: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
}

export default AdvertisingPage

export const pageQuery = graphql`
    query AdvertisingPageQuery {
        site {
            siteMetadata {
                languages {
                    defaultLangKey
                    langs
                }
            }
        }
        allArticlesJson(filter: { title: { eq: "home" } }) {
            edges {
                node {
                    articles {
                        en
                        ka
                    }
                }
            }
        }
        markdownRemark {
            html
            frontmatter {
                id
                title
            }
            fields {
                slug
            }
        }
    }
`
