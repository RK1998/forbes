---
id: '06'
templateKey: category-post
title: This Electric Porsche 911 Has A Smartphone-Controlled Tailpipe
date: 2019-02-06T22:31:28.375Z
description: My first gatsby category post really...
image: /img/porsche.jpg
imageAlt: alter
lang: en
path: /en/category/2021-05-30-this-electric-porsche-911-has-a-smartphone-controlled-tailpipe/
slug: /en/category/2021-05-30-this-electric-porsche-911-has-a-smartphone-controlled-tailpipe/
tags:
    - Technology
author: Alistair Charlton
authorImage: /img/avatar.jpg
authorPosition: Senior Contributor
authorQuote: I write about the automotive industry and where it is headed next.
authorLinkedIn: https://www.linkedin.com/in/makhokituashvili/
---

Everrati, a UK-based electric car specialist, this week revealed its latest creation, a 964-generation Porsche 911.

The car is powered by a 53kWh battery pack, has a claimed range of over 150 miles, and outputs 500 horsepower.

It weighs slightly less than when it was powered by internal combustion – partly thanks to the use of carbon fiber body panels – and can sprint to 60mph in under four seconds. DC fast charging means the battery can be topped up from 10 percent to almost full in an hour.

But what sets this car apart from a growing range of electrified classics is the pair of tailpipes sprouting from under the rear bumper. Linked to a smartphone application, the mock exhaust emits a sound reminiscent of the flat-six engine the Porsche was born with.
