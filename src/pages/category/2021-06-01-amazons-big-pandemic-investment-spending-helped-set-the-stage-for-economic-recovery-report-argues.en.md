---
id: '09'
templateKey: category-post
title: Is it the right time 2nd-post for the starting new digital agency company?
date: 2019-02-06T22:31:29.375Z
description: My first gatsby category post really...
image: /img/articles/amazon.jpg
imageAlt: alter
lang: en
path: /en/category/2021-06-01-amazons-big-pandemic-investment-spending-helped-set-the-stage-for-economic-recovery-report-argues
slug: /en/category/2021-06-01-amazons-big-pandemic-investment-spending-helped-set-the-stage-for-economic-recovery-report-argues
tags:
    - Technology
author: Makho Kituashvili
authorImage: /img/avatar.jpg
authorPosition: Forbes Staff
authorQuote: I cover breaking news.
authorLinkedIn: https://www.linkedin.com/in/makhokituashvili/
---

TOPLINE While other companies cut back on spending as the coronavirus pandemic took hold last year, e-commerce giant Amazon boosted its domestic capital investments by 75% to nearly $34 billion—and helped set the stage for a robust economic recovery, according to a new report from the Progressive Policy Institute.
