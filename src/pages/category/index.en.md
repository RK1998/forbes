---
id: '02'
templateKey: category-index
title: Category
date: 2019-03-24T00:00:00.000Z
description: Index page for all category posts.
image: /img/PersimmonHD.jpg
imageAlt: alter
lang: en
path: /en/category/
author: Colin M Donhue
authorImage: /img/avatar.jpg
authorPosition: Co-Founder & CEO
authorQuote: >-
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua.
authorLinkedIn: 'https://www.linkedin.com/in/colinmdonohue/'
slug: /en/category/
---

body
