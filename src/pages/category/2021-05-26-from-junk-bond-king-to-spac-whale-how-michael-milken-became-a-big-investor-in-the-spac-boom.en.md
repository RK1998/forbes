---
id: '11'
templateKey: category-post
title: 'From Junk Bond King To SPAC Whale: How Michael Milken Became A Big Investor In The SPAC Boom'
date: 2019-02-06T22:31:19.375Z
description: My 3-rd gatsby category post really...
image: /img/articles/junk.jpg
imageAlt: alter
lang: en
path: /en/category/2021-05-26-from-junk-bond-king-to-spac-whale-how-michael-milken-became-a-big-investor-in-the-spac-boom
slug: /en/category/2021-05-26-from-junk-bond-king-to-spac-whale-how-michael-milken-became-a-big-investor-in-the-spac-boom
tags:
    - Technology
author: Makho Kituashvili
authorImage: /img/avatar.jpg
authorPosition: Co-Founder
authorQuote: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
    eiusmod tempor incididunt ut labore et dolore magna aliqua.
authorLinkedIn: https://www.linkedin.com/in/makhokituashvili/
---

Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
