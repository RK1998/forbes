---
id: '02'
templateKey: category-index
title: ბლოგი
date: 2019-03-24T00:00:00.000Z
description: Pagina indice per tutti i Category posts.
image: /img/PersimmonHD.jpg
imageAlt: index
lang: ka
path: /ka/Category/
author: Makho Kituashvili
authorImage: /img/avatar.jpg
authorPosition: Co-Founder
authorQuote: >-
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua.
authorLinkedIn: 'https://www.linkedin.com/in/makhokituashvili/'
slug: /ka/Category/
---

index
