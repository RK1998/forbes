import React from 'react'
import FaLinkedin from '@meronex/icons/fa/FaLinkedin'
import FaFacebookSquare from '@meronex/icons/fa/FaFacebookSquare'
import FaInstagramSquare from '@meronex/icons/fa/FaInstagramSquare'
import FaYoutubeSquare from '@meronex/icons/fa/FaYoutubeSquare'
import BisShareAlt from '@meronex/icons/bi/BisShareAlt'
import BilFacebook from '@meronex/icons/bi/BilFacebook'
import BilTwitter from '@meronex/icons/bi/BilTwitter'
import BilLinkedin from '@meronex/icons/bi/BilLinkedin'
import BilPinterest from '@meronex/icons/bi/BilPinterest'
import BsFillEnvelopeFill from '@meronex/icons/bs/BsFillEnvelopeFill'
import Layout from '../components/LayoutFixed'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

const PressRoomPage = ({ data, location }) => {
    const jsonData = data.allArticlesJson.edges[0].node.articles
    return (
        <Layout data={data} jsonData={jsonData} location={location}>
            <div className="flex flex-col justify-center bg-white items-center">
                <div className="max-w-screen-md lg:px-10 my-8 mx-4">
                    <h1 className="text-turmeric-600 text-4xl font-serif font-bold">
                        Forbes Monaco Corporate Communications
                    </h1>
                    <p className="text-gray-700 my-5">
                        We publish press releases and news about the company.
                        For members of the press: if you're interested in
                        interviewing a Forbes reporter or contributor or
                        learning more about Forbes Monaco, please contact –{' '}
                        <a
                            className="text-turmeric-600 hover:text-turmeric-500"
                            href="#"
                        >
                            admin@forbes-monaco.com
                        </a>
                        . Follow us on Instagram @forbesmonaco
                    </p>
                    <div className="flex">
                        <a
                            title="facebook"
                            href="https://www.facebook.com/optimaldigitalservices"
                        >
                            <FaFacebookSquare
                                className="mx-2 bg-white text-black "
                                size="2.5rem"
                            />
                        </a>
                        <a
                            title="twitter"
                            href="https://twitter.com/optimal_digital"
                        >
                            <FaInstagramSquare
                                className="mx-2 bg-white text-black "
                                size="2.5rem"
                            />
                        </a>
                        <a
                            title="linkedin"
                            href="https://www.linkedin.com/company/optimal-digital-services"
                        >
                            <FaLinkedin
                                className="mx-2 bg-white text-black "
                                size="2.5rem"
                            />
                        </a>
                        <a
                            title="linkedin"
                            href="https://www.linkedin.com/company/optimal-digital-services"
                        >
                            <FaYoutubeSquare
                                className="mx-2 bg-white text-black "
                                size="2.5rem"
                            />
                        </a>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

PressRoomPage.propTypes = {
    data: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
}

export default PressRoomPage

export const pageQuery = graphql`
    query PressRoomPageQuery {
        site {
            siteMetadata {
                languages {
                    defaultLangKey
                    langs
                }
            }
        }
        allArticlesJson(filter: { title: { eq: "home" } }) {
            edges {
                node {
                    articles {
                        en
                        ka
                    }
                }
            }
        }
        markdownRemark {
            html
            frontmatter {
                id
                title
            }
            fields {
                slug
            }
        }
    }
`
