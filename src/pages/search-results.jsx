import React from 'react'
import Link from 'gatsby-link'
import ViewCounter from '../components/ViewCounter'

function SearchRoll() {
    return (
        <>
            <div id="mainTwoBogroll2" className="my-2 mx-5 lg:mx-20">
                <h1 className="text-3xl font-bold text-black border-turmeric-500 border-b-2 mb-4 pb-1">
                    Search Results
                </h1>
                <div
                    className=""
                    // remove this margin after pasting it in it's actual component
                >
                    <div className="flex flex-col md:flex-row my-3">
                        <div className="w-full md:w-8/12">
                            <h2>
                                <Link
                                    className="font-bold text-black hover:text-turmeric-600 text-2xl"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* {post.frontmatter.title} */}
                                    Lorem ipsum dolor sit amet consectetur
                                    adipisicing elit. Corrupti?
                                </Link>
                            </h2>

                            <div className="flex items-center my-2">
                                <div className="mr-3 items-center">
                                    <span className="text-xs">
                                        {/* <Time
                                                            pubdate
                                                            langKey={
                                                                post.fields
                                                                    .langKey
                                                            }
                                                            date={
                                                                post.frontmatter
                                                                    .date
                                                            }
                                                        /> */}
                                        26 jan, 2019
                                    </span>
                                </div>

                                <div className="">
                                    <div className="">
                                        <p className="border-l-2 pl-3 text-xs text-gray-500">
                                            by{' '}
                                            {/* {
                                                                    post
                                                                        .frontmatter
                                                                        .author
                                                                } */}
                                            Makho Kituashvili
                                        </p>
                                    </div>
                                </div>

                                <div className="flex mx-3">
                                    <svg
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="far"
                                        data-icon="eye"
                                        className="w-3 text-gray-500 mr-1"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                    >
                                        <path
                                            fill="currentColor"
                                            d="M288 144a110.94 110.94 0 0 0-31.24 5 55.4 55.4 0 0 1 7.24 27 56 56 0 0 1-56 56 55.4 55.4 0 0 1-27-7.24A111.71 111.71 0 1 0 288 144zm284.52 97.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400c-98.65 0-189.09-55-237.93-144C98.91 167 189.34 112 288 112s189.09 55 237.93 144C477.1 345 386.66 400 288 400z"
                                        ></path>
                                    </svg>
                                    <span className="text-xs">
                                        {/* <ViewCounter id="BlogPostTemplate" /> */}
                                        899
                                    </span>
                                </div>
                            </div>

                            <p className="">
                                <Link
                                    className="text-gray-500 hover:text-gray-500 text-sm"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* {post.excerpt} */}
                                    Contrary to popular belief, Lorem Ipsum is
                                    not simply random text. It has roots in a
                                    piece of classical Latin literature from 45
                                    BC, making it over…
                                </Link>
                            </p>

                            <div className="mt-5">
                                <Link
                                    className="text-turmeric-500 hover:text-turmeric-700 text-sm"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* <FormattedMessage id="blog.keep.reading" /> */}
                                    keep reading...
                                </Link>
                            </div>
                        </div>
                        <div className="w-full md:w-4/12">
                            <figure className="">
                                {/* <Img
                                                        alt={
                                                            post.frontmatter
                                                                .imageAlt
                                                        }
                                                        fluid={
                                                            post.frontmatter
                                                                .image
                                                                .childImageSharp
                                                                .fluid
                                                        }
                                                    /> */}
                                <img
                                    src="https://placeimg.com/400/300"
                                    alt=""
                                />
                            </figure>
                        </div>
                    </div>

                    <div className="flex flex-col md:flex-row my-3">
                        <div className="w-full md:w-8/12">
                            <h2>
                                <Link
                                    className="font-bold text-black hover:text-turmeric-600 text-2xl"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* {post.frontmatter.title} */}
                                    Lorem ipsum dolor sit amet consectetur
                                    adipisicing elit. Corrupti?
                                </Link>
                            </h2>

                            <div className="flex items-center my-2">
                                <div className="mr-3 items-center">
                                    <span className="text-xs">
                                        {/* <Time
                                                            pubdate
                                                            langKey={
                                                                post.fields
                                                                    .langKey
                                                            }
                                                            date={
                                                                post.frontmatter
                                                                    .date
                                                            }
                                                        /> */}
                                        26 jan, 2019
                                    </span>
                                </div>

                                <div className="">
                                    <div className="">
                                        <p className="border-l-2 pl-3 text-xs text-gray-500">
                                            by{' '}
                                            {/* {
                                                                    post
                                                                        .frontmatter
                                                                        .author
                                                                } */}
                                            Makho Kituashvili
                                        </p>
                                    </div>
                                </div>

                                <div className="flex mx-3">
                                    <svg
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="far"
                                        data-icon="eye"
                                        className="w-3 text-gray-500 mr-1"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                    >
                                        <path
                                            fill="currentColor"
                                            d="M288 144a110.94 110.94 0 0 0-31.24 5 55.4 55.4 0 0 1 7.24 27 56 56 0 0 1-56 56 55.4 55.4 0 0 1-27-7.24A111.71 111.71 0 1 0 288 144zm284.52 97.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400c-98.65 0-189.09-55-237.93-144C98.91 167 189.34 112 288 112s189.09 55 237.93 144C477.1 345 386.66 400 288 400z"
                                        ></path>
                                    </svg>
                                    <span className="text-xs">
                                        {/* <ViewCounter id="BlogPostTemplate" /> */}
                                        899
                                    </span>
                                </div>
                            </div>

                            <p className="">
                                <Link
                                    className="text-gray-500 hover:text-gray-500 text-sm"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* {post.excerpt} */}
                                    Contrary to popular belief, Lorem Ipsum is
                                    not simply random text. It has roots in a
                                    piece of classical Latin literature from 45
                                    BC, making it over…
                                </Link>
                            </p>

                            <div className="mt-5">
                                <Link
                                    className="text-turmeric-500 hover:text-turmeric-700 text-sm"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* <FormattedMessage id="blog.keep.reading" /> */}
                                    keep reading...
                                </Link>
                            </div>
                        </div>
                        <div className="w-full md:w-4/12">
                            <figure className="">
                                {/* <Img
                                                        alt={
                                                            post.frontmatter
                                                                .imageAlt
                                                        }
                                                        fluid={
                                                            post.frontmatter
                                                                .image
                                                                .childImageSharp
                                                                .fluid
                                                        }
                                                    /> */}
                                <img
                                    src="https://placeimg.com/400/300"
                                    alt=""
                                />
                            </figure>
                        </div>
                    </div>

                    <div className="flex flex-col md:flex-row my-3">
                        <div className="w-full md:w-8/12">
                            <h2>
                                <Link
                                    className="font-bold text-black hover:text-turmeric-600 text-2xl"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* {post.frontmatter.title} */}
                                    Lorem ipsum dolor sit amet consectetur
                                    adipisicing elit. Corrupti?
                                </Link>
                            </h2>

                            <div className="flex items-center my-2">
                                <div className="mr-3 items-center">
                                    <span className="text-xs">
                                        {/* <Time
                                                            pubdate
                                                            langKey={
                                                                post.fields
                                                                    .langKey
                                                            }
                                                            date={
                                                                post.frontmatter
                                                                    .date
                                                            }
                                                        /> */}
                                        26 jan, 2019
                                    </span>
                                </div>

                                <div className="">
                                    <div className="">
                                        <p className="border-l-2 pl-3 text-xs text-gray-500">
                                            by{' '}
                                            {/* {
                                                                    post
                                                                        .frontmatter
                                                                        .author
                                                                } */}
                                            Makho Kituashvili
                                        </p>
                                    </div>
                                </div>

                                <div className="flex mx-3">
                                    <svg
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="far"
                                        data-icon="eye"
                                        className="w-3 text-gray-500 mr-1"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                    >
                                        <path
                                            fill="currentColor"
                                            d="M288 144a110.94 110.94 0 0 0-31.24 5 55.4 55.4 0 0 1 7.24 27 56 56 0 0 1-56 56 55.4 55.4 0 0 1-27-7.24A111.71 111.71 0 1 0 288 144zm284.52 97.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400c-98.65 0-189.09-55-237.93-144C98.91 167 189.34 112 288 112s189.09 55 237.93 144C477.1 345 386.66 400 288 400z"
                                        ></path>
                                    </svg>
                                    <span className="text-xs">
                                        {/* <ViewCounter id="BlogPostTemplate" /> */}
                                        899
                                    </span>
                                </div>
                            </div>

                            <p className="">
                                <Link
                                    className="text-gray-500 hover:text-gray-500 text-sm"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* {post.excerpt} */}
                                    Contrary to popular belief, Lorem Ipsum is
                                    not simply random text. It has roots in a
                                    piece of classical Latin literature from 45
                                    BC, making it over…
                                </Link>
                            </p>

                            <div className="mt-5">
                                <Link
                                    className="text-turmeric-500 hover:text-turmeric-700 text-sm"
                                    // to={post.fields.slug}
                                    to={'/'}
                                >
                                    {/* <FormattedMessage id="blog.keep.reading" /> */}
                                    keep reading...
                                </Link>
                            </div>
                        </div>
                        <div className="w-full md:w-4/12">
                            <figure className="">
                                {/* <Img
                                                        alt={
                                                            post.frontmatter
                                                                .imageAlt
                                                        }
                                                        fluid={
                                                            post.frontmatter
                                                                .image
                                                                .childImageSharp
                                                                .fluid
                                                        }
                                                    /> */}
                                <img
                                    src="https://placeimg.com/400/300"
                                    alt=""
                                />
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default SearchRoll
