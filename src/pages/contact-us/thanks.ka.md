---
id: '08'
title: 'Thank you'
description: 'Thank you'
templateKey: message
lang: ka
date: '31-03-2019'
path: /ka/contact-us/thanks/
slug: /ka/contact-us/thanks/
---

Thank you! You will be contacted as soon as possible, in the meantime we wish you a good visit and a good continuation!
