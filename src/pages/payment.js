import React from 'react'
import Link from 'gatsby-link'
import LgPaypal from '@meronex/icons/lg/LgPaypal'
import AiOutlineCreditCard from '@meronex/icons/ai/AiOutlineCreditCard'

function Payment() {
    return (
        <div>
            <div>
                <div className="mx-10 lg:mx-40">
                    <h1 className="text-4xl text-black font-serif font-semibold">
                        Please enter personal information
                    </h1>
                    <p className="font-serif text-gray-600">
                        Your information will be used in accordance with our{' '}
                        <Link
                            className="text-turmeric-600 hover:text-turmeric-500"
                            to="/privacy-policy"
                        >
                            Privacy Policy
                        </Link>
                    </p>

                    <h2 className="mt-10 font-serif font-bold text-xl">
                        Enter your details below
                    </h2>
                    <p className="text-gray-500 mb-14">* required field</p>
                </div>
                <div
                    id="payment-form"
                    className="mx-10 w-full max-w-lg sm:mx-auto"
                >
                    <div className="flex flex-col sm:flex-row sm:justify-between my-4">
                        <label htmlFor="name" className="text-gray-600 text-lg">
                            Name <sup>*</sup>{' '}
                        </label>
                        <input
                            className="border-gray-300 bg-gray-200 border outline-none w-80 max-w-md focus:ring-1 ring-gray-500 placeholder-opacity-80 placeholder-gray-500 pl-2 font-serif"
                            type="text"
                            placeholder="Name (Letters Only)"
                        />
                    </div>
                    <div className="flex flex-col sm:flex-row sm:justify-between my-4">
                        <label
                            htmlFor="companyName"
                            className="text-gray-600 text-lg"
                        >
                            Company Name{' '}
                        </label>
                        <input
                            className="border-gray-300 bg-gray-200 border outline-none w-80 max-w-md focus:ring-1 ring-gray-500 placeholder-opacity-80 placeholder-gray-500 pl-2 font-serif"
                            type="text"
                            placeholder="Company Name (Letters Only)"
                        />
                    </div>
                    <div className="flex flex-col sm:flex-row sm:justify-between my-4">
                        <label
                            htmlFor="address"
                            className="text-gray-600 text-lg"
                        >
                            Address <sup>*</sup>{' '}
                        </label>
                        <input
                            className="border-gray-300 bg-gray-200 border outline-none w-80 max-w-md focus:ring-1 ring-gray-500 placeholder-opacity-80 placeholder-gray-500 pl-2 font-serif"
                            type="text"
                            placeholder="Address (Letters Only)"
                        />
                    </div>
                    <div className="flex flex-col sm:flex-row sm:justify-between my-4">
                        <label htmlFor="zip" className="text-gray-600 text-lg">
                            Zip/City <sup>*</sup>{' '}
                        </label>
                        <input
                            className="border-gray-300 bg-gray-200 border outline-none w-80 max-w-md focus:ring-1 ring-gray-500 placeholder-opacity-80 placeholder-gray-500 pl-2 font-serif"
                            type="text"
                            placeholder="Zip/City (Letters Only)"
                        />
                    </div>
                    <div className="flex flex-col sm:flex-row sm:justify-between my-4">
                        <label
                            htmlFor="email"
                            className="text-gray-600 text-lg"
                        >
                            Email <sup>*</sup>{' '}
                        </label>
                        <input
                            className="border-gray-300 bg-gray-200 border outline-none w-80 max-w-md focus:ring-1 ring-gray-500 placeholder-opacity-80 placeholder-gray-500 pl-2 font-serif"
                            type="text"
                            placeholder="email@example.co.uk"
                        />
                    </div>
                    <div className="flex flex-col my-4">
                        <button className="w-80 my-2 max-w-md sm:w-full sm:max-w-xl bg-turmeric-600 flex justify-center text-3xl p-2">
                            <LgPaypal />
                        </button>
                        <button className="w-80 my-2 max-w-md sm:w-full sm:max-w-xl bg-gray-900 flex justify-center text-3xl p-2">
                            <AiOutlineCreditCard className="text-white"></AiOutlineCreditCard>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Payment
