---
id: '23'
templateKey: work-post
number: '1'
title: Geva Motors
projectDate: December 2019
projectTimeline: 4 weeks - 2 sprints (2 weeks each)
projectCity: Tbilisi, Georgia
projectMembers: 1 Front-end engineer <br> 1 Full-stack engineer
projectServices: Full Web Development <br> CMS integration <br> SEO optimization
    <br> Progressive Web Apps (PWA) <br> PPC & Project management
projectTools: HTML <br> Sass <br> React <br> GatsbyJS <br> Netlify CMS <br>
    Netlify <br> TypeScript <br>  Webpack <br> Bulma <br> i18n
projectFeatures: ADS remarketing <br> Contact form <br> Sale funnel <br>
    Internationalization <br> Call request
date: 2020-06-18T20:50:17.989Z
projectIndustry: Automotive
description: Geva Motors provides a 1500+ range of high-quality aftermarket
    parts for trucks and cars. The goal of Geva Motors is to increase customer
    value and decrease complexity from the process. The company is at an early
    stage of development, established the middle of 2018 and increase from 1 to 20
    employees under 12 months.
image: /img/gevamotors_front.png
imageAlt: Geva Motors
lang: en
path: /en/work/gevamotors
slug: /en/work/gevamotors
workDate: Automotive
---

<div class="has-text-weight-medium">
Our responsibilities were
</div>
<div class="has-text-dark">
Migrating platform to Netlify
<br>
Technology stack to ensure performance and scalability
<br>
Website speed under 1,5 seconds
<br>
Internationalization website & CMS panel
<br>
Setup tools for digital marketing
<br>
ADS remarketing
<br>
SEO optimization
<br>
Create sale funnel
<br>
Call request notification API Code quality => 90
</div>

<div class="has-text-weight-medium mt-4">
Solution
</div>
<div class="has-text-dark">
Our team wanted visitors to be able to use the web app on the go via mobile devices too, which is why we made it mobile friendly by PWA technology.
We executed the necessary components for gathering metrics about visitor engagement and detailed statistical reports for increasing step by step conversion rate. We developed a full-featured version of the web site, structured to increase value and decrease the complexity of the processes.
</div>

<div class="has-text-weight-medium mt-4">
What we provided
</div>
<div class="has-text-dark">
Restructured technological stack and make it more scalable.
<br>
Integrate headless CMS panel.
<br>
Developed internationalization support.
<br>
Implemented a marketing tool for an increased conversion rate.
<br>
Creating a reporting tool to see more accurate analytical reports.
</div>

<div class="has-text-weight-medium mt-4">
Results
</div>
<div class="has-text-dark">
We offered technological tools to save time, reduce efforts and costs for adding new functionalities, and sizing the application together with delivering clean, quality code on TypeScript.
 The right customer-focused marketing tools, designed to improve customer acquisition – which is helping to grow sales and closing deals by new sales channels.
</div>
