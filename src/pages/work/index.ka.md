---
templateKey: work-index
path: /ka/work/
slug: /ka/work/
id: '15'
title: work index page
number: '00'
workDate: Automotive
date: 2019-03-24T00:00:00.000Z
imageAlt: optimal digital
lang: ka
description: Index page for all work posts.
image: /img/index.png
---

body
